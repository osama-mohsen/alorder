package app.grand.tafwak.data.product.local

import app.grand.tafwak.domain.home.models.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ProductLocalRepositoryImpl @Inject constructor(private val productLocalDataSource: ProductLocalDataSource) :
  ProductLocalRepository {

  override suspend fun products(): Flow<List<Product>> = productLocalDataSource.getProductLocal()

  override suspend fun getProduct(id: Int): Flow<Product> = productLocalDataSource.getProduct(id)

  override suspend fun insert(product: Product) = productLocalDataSource.insert(product)
  override suspend fun isExist(id: Int): Flow<Int> = productLocalDataSource.isExist(id)
  override suspend fun getProductsCount(): Flow<Int> = productLocalDataSource.getProductsCount()
  override suspend fun getProductsTotal(): Flow<Double> = productLocalDataSource.getProductsTotal()
  override suspend fun find(id: Int): Flow<Product> = productLocalDataSource.find(id)

  override suspend fun delete(id: Int) = productLocalDataSource.deleteProduct(id)
  override suspend fun removeCompany(id: Int) = productLocalDataSource.removeCompany(id)

  override suspend fun updateProduct(id: Int) = productLocalDataSource.update(id)
  override suspend fun clear() = productLocalDataSource.clear()

}
package app.grand.tafwak.data.home.data_source.remote

import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.order.OrderDetailsResponse
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.order.SendComplainRequest
import app.grand.tafwak.domain.utils.BaseResponse
import retrofit2.http.*

interface HomeServices {

  @GET("users/getHomepage")
  suspend fun home(): HomeResponse


  @GET("users/token")
  suspend fun updateToken(): BaseResponse


  @GET("orders/getUserOrders")
  suspend fun orders(): OrderListResponse


  @GET("orders/{id}")
  suspend fun getOrderDetails(@Path("id") id: Int): OrderDetailsResponse

  @GET("categories/{id}")
  suspend fun getCategoryDetails(
    @Path("id") id: Int,
    @Query("productsPage") page: Int,
    @Query("productsLimit") limit: Int
  ): CategoryDetailsResponse


  @GET("products")
  suspend fun getProducts(
    @Query("category_id") categoryId: Int?,
    @Query("search") search: String?,
    @Query("company_id") companyId: Int?,
    @Query("featured") featured: Int?,
    @Query("page") page: Int,
    @Query("limit") limit: Int
  ): ProductsResponse


  @GET("companies")
  suspend fun companies(@Query("search") search: String): CompaniesResponse

  @GET("companies/categories")
  suspend fun companiesCategories(@Query("company_id") companyId: Int): CompaniesCategoriesResponse


  @POST("orders/create")
  suspend fun submitCart(@Body request: ArrayList<Cart>): BaseResponse


  @POST("users/confirmPassword")
  suspend fun submitPassword(@Body request: ConfirmPasswordRequest): BaseResponse



  @POST("orders/confirmDelivery/{id}")
  suspend fun confirmDeliver(@Path("id") id: Int): BaseResponse

  @POST("complaint/send")
  suspend fun sendComplain(
    @Body request: ComplainRequest
  ): BaseResponse

}
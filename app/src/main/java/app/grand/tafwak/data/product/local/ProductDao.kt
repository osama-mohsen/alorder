package app.grand.tafwak.data.product.local

import androidx.room.*
import app.grand.tafwak.domain.home.models.CompanyCart
import app.grand.tafwak.domain.home.models.Product
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductDao {
  @Query("Select * from product")
   fun getProducts(): Flow<List<Product>>

  @Query("Select * from product WHERE id=:id LIMIT 1")
  fun getProduct(id: Int): Flow<Product>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
   fun insertProduct(product: Product)

  @Query("UPDATE product SET quantity = quantity+1 WHERE id = :id")
   fun updateProduct(id: Int)

  @Query("UPDATE product SET quantity = quantity-1 WHERE id = :id")
  fun decreaseProduct(id: Int)

  @Query("SELECT COUNT (product.id) FROM product WHERE id = :id")
  fun isExist(id: Int) : Flow<Int>

  @Query("SELECT COUNT (product.id) FROM product")
  fun getProductsCount() : Flow<Int>

  @Query("SELECT SUM (product.price) FROM product")
  fun getProductsTotal() : Flow<Double>

  @Query("SELECT product.quantity FROM product WHERE id = :id")
  fun productQuantity(id: Int) : Flow<Int>

  @Query("SELECT * FROM product WHERE id = :id")
   fun find(id: Int): Flow<Product>

  @Query("DELETE FROM product")
  fun clear()

  @Query("DELETE FROM product WHERE id = :id")
   fun delete(id: Int)


  @Query("DELETE FROM product WHERE product.companyId = :id")
  fun removeCompany(id: Int)

}
package app.grand.tafwak.data.profile.data_source

import android.net.Uri
import app.grand.tafwak.core.MyApplication
import app.grand.tafwak.core.extenstions.createMultipartBodyPart
import app.grand.tafwak.data.remote.BaseRemoteDataSource
import app.grand.tafwak.domain.profile.entity.UpdateProfileRequest
import okhttp3.MultipartBody
import javax.inject.Inject

class ProfileDataSource @Inject constructor(private val apiService: ProfileServices) :
  BaseRemoteDataSource() {

  suspend fun updateProfile(request: UpdateProfileRequest) = safeApiCall {
    apiService.updateProfile(request)
  }

  suspend fun updateImage(uri: Uri) = safeApiCall {
    apiService.updateImage(uri.createMultipartBodyPart(MyApplication.instance, "image"))
  }

  suspend fun avatarList() = safeApiCall {
    apiService.avatarList()
  }

}
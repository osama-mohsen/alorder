package app.grand.tafwak.data.auth.repository

import android.net.Uri
import app.grand.tafwak.data.auth.data_source.remote.AuthRemoteDataSource
import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.auth.repository.AuthRepository
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.profile.entity.UpdatePassword
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
  private val remoteDataSource: AuthRemoteDataSource
) : AuthRepository {

  override
  suspend fun logIn(request: LogInRequest) = remoteDataSource.logIn(request)


  override suspend fun changePasswordNotAuthorized(request: ChangePasswordRequest): Resource<BaseResponse> =
    remoteDataSource.changePasswordNotAuthorized(request)

  override suspend fun changePasswordAuthorized(request: ChangePasswordRequest): Resource<BaseResponse> =
    remoteDataSource.changePasswordAuthorized(request)


  override suspend fun updatePassword(request: UpdatePassword): Resource<BaseResponse> =
    remoteDataSource.updatePassword(request)

  override suspend fun getGovernorates(): Resource<GovernoratesResponse> =
    remoteDataSource.getGovernorates()

  override suspend fun getDeferredPayments(): Resource<DeferredPaymentsResponse> =
    remoteDataSource.getDeferredPayments()


  override suspend fun payDeferred(request: PayDeferredRequest): Resource<BaseResponse> =
    remoteDataSource.payDeferred(request)

  override suspend fun getCities(id: Int): Resource<CitiesResponse> =
    remoteDataSource.getCities(id)

  override suspend fun forgetPassword(request: ForgetPasswordRequest) =
    remoteDataSource.forgetPassword(request)

  override suspend fun register(request: RegisterRequest): Resource<BaseResponse> =
    remoteDataSource.register(request)


  override suspend fun addAddress(request: AddAddress): Resource<UserResponse> =
    remoteDataSource.addAddress(request)

  override suspend fun verifyAccount(request: VerifyAccountRequest): Resource<BaseResponse> =
    remoteDataSource.verifyAccount(request)

}
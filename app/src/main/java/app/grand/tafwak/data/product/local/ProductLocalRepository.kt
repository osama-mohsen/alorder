package app.grand.tafwak.data.product.local

import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

interface ProductLocalRepository {
  suspend fun products(): Flow<List<Product>>
  suspend fun getProduct(id: Int): Flow<Product>
  suspend fun insert(product: Product)
  suspend fun isExist(id: Int): Flow<Int>
  suspend fun getProductsCount(): Flow<Int>
  suspend fun getProductsTotal(): Flow<Double>
  suspend fun find(id: Int): Flow<Product>
  suspend fun delete(id: Int)
  suspend fun removeCompany(companyId: Int)
  suspend fun updateProduct(id: Int)
  suspend fun clear()
}
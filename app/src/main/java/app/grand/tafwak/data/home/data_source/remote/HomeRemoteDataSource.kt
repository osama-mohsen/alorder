package app.grand.tafwak.data.home.data_source.remote

import app.grand.tafwak.data.remote.BaseRemoteDataSource
import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.order.SendComplainRequest
import app.grand.tafwak.presentation.base.utils.Constants
import javax.inject.Inject

class HomeRemoteDataSource @Inject constructor(private val apiService: HomeServices) :
  BaseRemoteDataSource() {

  suspend fun home() = safeApiCall {
    apiService.home()
  }

  suspend fun updateToken() = safeApiCall {
    apiService.updateToken()
  }
  suspend fun orders() = safeApiCall {
    apiService.orders()
  }


  suspend fun getOrderDetails(id: Int) = safeApiCall {
    apiService.getOrderDetails(id)
  }
  suspend fun submitCart(request: ArrayList<Cart>) = safeApiCall {
    apiService.submitCart(request)
  }
  suspend fun submitPassword(request: ConfirmPasswordRequest) = safeApiCall {
    apiService.submitPassword(request)
  }



  suspend fun sendComplain(request: ComplainRequest) = safeApiCall {
    apiService.sendComplain(request)
  }

  suspend fun confirmDeliver(request: Int) = safeApiCall {
    apiService.confirmDeliver(request)
  }

  suspend fun companies(search: String) = safeApiCall {
    apiService.companies(search)
  }

  suspend fun companiesCategories(companyId: Int) = safeApiCall {
    apiService.companiesCategories(companyId)
  }



  suspend fun categoryDetails(id: Int, page: Int) = safeApiCall {
    apiService.getCategoryDetails(id,page,Constants.PAGINATION_LIMIT)
  }
  suspend fun products(categoryId: Int?,search:String?, companyId: Int?,featured: Int?, page: Int) = safeApiCall {
    apiService.getProducts(categoryId,search,companyId,featured,page,Constants.PAGINATION_LIMIT)
  }
}
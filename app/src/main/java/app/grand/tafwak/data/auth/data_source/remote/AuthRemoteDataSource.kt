package app.grand.tafwak.data.auth.data_source.remote

import android.net.Uri
import app.grand.tafwak.core.MyApplication
import app.grand.tafwak.core.extenstions.createMultipartBodyPart
import app.grand.tafwak.data.remote.BaseRemoteDataSource
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.profile.entity.UpdatePassword
import javax.inject.Inject

class AuthRemoteDataSource @Inject constructor(private val apiService: AuthServices) :
  BaseRemoteDataSource() {


  suspend fun logIn(request: LogInRequest) = safeApiCall {
    apiService.logIn(request)
  }
  suspend fun addAddress(request: AddAddress) = safeApiCall {
    apiService.addAddress(request)
  }

  suspend fun forgetPassword(request: ForgetPasswordRequest) = safeApiCall {
    apiService.ForgetPassword(request)
  }

  suspend fun verifyAccount(request: VerifyAccountRequest) = safeApiCall {
    apiService.verifyAccount(request)
  }

  suspend fun changePasswordNotAuthorized(request: ChangePasswordRequest) = safeApiCall {
    apiService.changePasswordNotAuthorize(request)
  }

  suspend fun changePasswordAuthorized(request: ChangePasswordRequest) = safeApiCall {
    apiService.changePasswordAuthorize(request)
  }

  suspend fun updatePassword(request: UpdatePassword) = safeApiCall {
    apiService.updatePassword(request)
  }

  suspend fun getGovernorates() = safeApiCall {
    apiService.getGovernorates()
  }

  suspend fun getDeferredPayments() = safeApiCall {
    apiService.getDeferredPayments()
  }

  suspend fun payDeferred(request:PayDeferredRequest) = safeApiCall {
    apiService.payDeferred(request)
  }

  suspend fun getCities(id: Int) = safeApiCall {
    apiService.getCities(id)
  }



  suspend fun register(request: RegisterRequest) = safeApiCall {
    apiService.register(request)
  }


}
package app.grand.tafwak.data.home.repository

import app.grand.tafwak.data.home.data_source.remote.HomeRemoteDataSource
import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.home.repository.HomeRepository
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.order.OrderDetailsResponse
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(private val remoteDataSource: HomeRemoteDataSource) :
  HomeRepository {
  override suspend fun home(): Resource<HomeResponse> =
    remoteDataSource.home()
  override suspend fun updateToken(): Resource<BaseResponse> =
    remoteDataSource.updateToken()
  override suspend fun orders(): Resource<OrderListResponse> =
    remoteDataSource.orders()
  override suspend fun getOrderDetails(id:Int): Resource<OrderDetailsResponse> =
    remoteDataSource.getOrderDetails(id)
  override suspend fun companies(search: String): Resource<CompaniesResponse> =
    remoteDataSource.companies(search)
  override suspend fun companiesCategories(companyId: Int): Resource<CompaniesCategoriesResponse> =
    remoteDataSource.companiesCategories(companyId)


  override suspend fun submitCart(request: ArrayList<Cart>): Resource<BaseResponse> =
    remoteDataSource.submitCart(request)

  override suspend fun submitPassword(request: ConfirmPasswordRequest): Resource<BaseResponse> =
    remoteDataSource.submitPassword(request)



  override suspend fun confirmDeliver(request: Int): Resource<BaseResponse> =
    remoteDataSource.confirmDeliver(request)

  override suspend fun sendComplain(request: ComplainRequest): Resource<BaseResponse> =
    remoteDataSource.sendComplain(request)

  override suspend fun categoryDetails(id: Int,page: Int): Resource<CategoryDetailsResponse> =
    remoteDataSource.categoryDetails(id,page)
  override suspend fun products(categoryId: Int?,search: String?, companyId: Int?,featured: Int?, page: Int): Resource<ProductsResponse> =
    remoteDataSource.products(categoryId,search,companyId,featured,page)
}
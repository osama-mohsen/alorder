package app.grand.tafwak.data.profile.data_source

import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.profile.entity.AvatarResponse
import app.grand.tafwak.domain.profile.entity.UpdateProfileRequest
import app.grand.tafwak.domain.utils.BaseResponse
import okhttp3.MultipartBody
import retrofit2.http.*

interface ProfileServices {

  @POST("users/updateProfile")
  suspend fun updateProfile(@Body request: UpdateProfileRequest): BaseResponse

  @GET("v1/avatars")
  suspend fun avatarList(): ArrayList<AvatarResponse>


  @Multipart
  @POST("users/uploadProfileImg")
  suspend fun updateImage(
    @Part image: MultipartBody.Part?
  ): UserResponse


}
package app.grand.tafwak.data.auth.data_source.remote

import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.ForgetPasswordResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.profile.entity.UpdatePassword
import app.grand.tafwak.domain.utils.BaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface AuthServices {

  @POST("users/login")
  suspend fun logIn(@Body request: LogInRequest): UserResponse

  @POST("users/addAddress")
  suspend fun addAddress(@Body request: AddAddress): UserResponse

  @POST("users/resetPassword")
  suspend fun ForgetPassword(@Body request: ForgetPasswordRequest): ForgetPasswordResponse

  @POST("users/resetPassword/verifyPhone")
  suspend fun verifyAccount(@Body request: VerifyAccountRequest): BaseResponse

  @POST("users/resetPassword/updatePassword")
  suspend fun changePasswordNotAuthorize(@Body request: ChangePasswordRequest): BaseResponse

  @POST("users/updateProfile/changePassword")
  suspend fun changePasswordAuthorize(@Body request: ChangePasswordRequest): BaseResponse

  @POST("users/resetPassword/updatePassword")
  suspend fun updatePassword(@Body request: UpdatePassword): BaseResponse

  @GET("governorate")
  suspend fun getGovernorates(): GovernoratesResponse


  @GET("users/getDeferredPayments")
  suspend fun getDeferredPayments(): DeferredPaymentsResponse


  @POST("users/payDeferredPayments")
  suspend fun payDeferred(@Body request:PayDeferredRequest): DeferredPaymentsResponse



  @GET("governorate/{id}")
  suspend fun getCities(@Path("id") id: Int): CitiesResponse


  @POST("users/register")
  suspend fun register(
    @Body request: RegisterRequest
  ): BaseResponse

}
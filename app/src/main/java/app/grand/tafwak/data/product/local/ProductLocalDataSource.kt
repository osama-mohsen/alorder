package app.grand.tafwak.data.product.local

import android.util.Log
import app.grand.tafwak.domain.home.models.Product
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.drop
import okhttp3.Dispatcher
import java.util.concurrent.Flow
import javax.inject.Inject

class ProductLocalDataSource @Inject constructor(private val productDao: ProductDao) {

  private val TAG = "ProductLocalDataSource"
  suspend fun getProductLocal() = productDao.getProducts()
  suspend fun getProduct(id: Int) = productDao.getProduct(id)
  suspend fun insert(productNew: Product) {
    withContext(Dispatchers.IO) {
      Log.d(TAG, "insert: $productNew")
//      if(productNew.quantity == 0) productNew.quantity = 1
      val product = isExist(productNew.id)
      product.collect {
        Log.e(TAG, "insert:$it")
        Log.e(TAG, "insert: Context")
        when (it) {
          0 -> productDao.insertProduct(productNew)
          else -> productDao.updateProduct(productNew.id)
        }
        Log.e(TAG, "insert: Cancel $productNew")
        cancel()
      }
    }
  }

  suspend fun update(id: Int) = productDao.updateProduct(id)
  suspend fun clear() = productDao.clear()


  //  suspend fun deleteProduct(id: Int) = productDao.delete(id)
  suspend fun deleteProduct(id: Int) {
    withContext(Dispatchers.IO) {
      val productExist = isExist(id)
      productExist.collect {
        Log.e(TAG, "insert:$it")
        Log.e(TAG, "insert: Context")
        if (it > 0) {
          Log.d(TAG, "deleteProduct: ${id}")
          withContext(Dispatchers.IO) {
            val product = find(id)
            product.collect {productItem ->
              Log.d(TAG, "deleteProduct: ${productItem.quantity}")
              when (productItem.quantity) {
                1 -> {
                  Log.d(TAG, "deleteProduct: 1")
                  withContext(Dispatchers.IO) {
                    productDao.delete(id)
                    cancel()
                  }
                }
                else -> {
                  withContext(Dispatchers.IO) {
                    productDao.decreaseProduct(id)
                    cancel()
                  }
                }
              }
            }
            cancel()
          }

        }
        Log.e(TAG, "insert: Cancel")
        cancel()
      }
    }
  }

  suspend fun removeCompany(companyId: Int) = productDao.removeCompany(companyId)

  suspend fun isExist(product_id: Int) = productDao.isExist(product_id)
  suspend fun getProductsCount() = productDao.getProductsCount()
  suspend fun getProductsTotal() = productDao.getProductsTotal()
  suspend fun find(product_id: Int) = productDao.find(product_id)

}
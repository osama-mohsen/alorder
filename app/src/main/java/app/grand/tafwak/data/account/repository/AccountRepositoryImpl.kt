package app.grand.tafwak.data.account.repository

import app.grand.tafwak.data.account.data_source.remote.AccountRemoteDataSource
import app.grand.tafwak.data.local.preferences.AppPreferences
import app.grand.tafwak.domain.account.entity.request.SendFirebaseTokenRequest
import app.grand.tafwak.domain.account.repository.AccountRepository
import app.grand.tafwak.domain.auth.entity.model.User
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AccountRepositoryImpl @Inject constructor(
  private val remoteDataSource: AccountRemoteDataSource,
  private val appPreferences: AppPreferences
) : AccountRepository {

  override suspend fun isLoggedIn(isLoggedIn: Boolean) {
    appPreferences.isLoggedIn(isLoggedIn)
  }

  override fun getIsLoggedIn(): Boolean {
    return appPreferences.getIsLoggedIn()
  }
  override  fun clearUser() {
    appPreferences.clearUser()
  }


  override fun saveUserToLocal(user: User) {
    appPreferences.saveUser(user)
  }

  override fun getKeyFromLocal(key: String): String = appPreferences.getLocal(key)

  override fun getUserToLocal(): User {
    return appPreferences.getUser()
  }

  override suspend fun saveUserToken(userToken: String) {
    appPreferences.userToken(userToken)
  }

  override suspend fun getUserToken(): Flow<String> {
    return appPreferences.getUserToken()
  }

  override fun saveKeyToLocal(key: String, value: String) {
    appPreferences.setLocal(key, value)
  }
  override suspend fun saveCountryId(country_id: String) {
    appPreferences.countryId(country_id)
  }

  override suspend fun getCountryId(): Flow<String> {
    return appPreferences.getCountryId()
  }

  override
  fun clearPreferences() = appPreferences.clearPreferences()
}
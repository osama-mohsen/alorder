package app.grand.tafwak.data.remote

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.ErrorResponse
import app.grand.tafwak.domain.utils.FailureStatus
import app.grand.tafwak.domain.utils.Resource
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import retrofit2.HttpException
import java.lang.Exception
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.HashMap
import javax.inject.Inject

open class BaseRemoteDataSource @Inject constructor() {
  var gson: Gson = Gson()
  private val TAG = "BaseRemoteDataSource"
  protected fun getParameters(requestData: Any): Map<String, String> {
    val params: MutableMap<String, String> = HashMap()
    try {
      val jsonObject = JSONObject(gson.toJson(requestData))
      for (i in 0 until jsonObject.names().length()) {
        params[jsonObject.names().getString(i)] =
          jsonObject[jsonObject.names().getString(i)].toString() + ""
      }
    } catch (e: Exception) {
      e.stackTrace
    }
    return params
  }

  suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T> {
    println(apiCall)
    Log.d(TAG, "safeApiCall: $apiCall")
    try {

      val apiResponse = apiCall.invoke()
//      val gson = Gson()
//      val type = object : TypeToken<ErrorResponse>() {}.type

      println(apiResponse)
      Log.d(TAG, "safeApiCall: ${apiResponse}")
      return when ((apiResponse as BaseResponse).success) {
          true -> {
            Log.d(TAG, "safeApiCall success")
            Resource.Success(apiResponse)
          }
          else -> {
            Log.d(TAG, "safeApiCall fail")

            Resource.Failure(FailureStatus.API_FAIL, 400,(apiResponse as BaseResponse).message)
          }
      }
    } catch (throwable: Throwable) {
      Log.d(TAG, "safeApiCall: ")
      Log.d(TAG, "safeApiCall ${throwable.message}")

      println(throwable)
      when (throwable) {
        is HttpException -> {
          when {
            throwable.code() == 422 -> {
              val jObjError = JSONObject(throwable.response()!!.errorBody()!!.string())
              val apiResponse = jObjError.toString()
              val response = Gson().fromJson(apiResponse, BaseResponse::class.java)

              return Resource.Failure(FailureStatus.API_FAIL, throwable.code(), response.message)
            }
//            throwable.code() == 401 -> {
//              val errorResponse = Gson().fromJson(
//                throwable.response()?.errorBody()!!.charStream().readText(),
//                ErrorResponse::class.java
//              )
//              return Resource.Failure(
//                FailureStatus.TOKEN_EXPIRED,
//                throwable.code(),
//                errorResponse.detail
//              )
//            }
            throwable.code() == 404 -> {
              val errorResponse = Gson().fromJson(
                throwable.response()?.errorBody()!!.charStream().readText(),
                ErrorResponse::class.java
              )

              return Resource.Failure(
                FailureStatus.API_FAIL,
                throwable.code(),
                errorResponse.detail
              )
            }
            throwable.code() == 500 -> {
              val errorResponse = Gson().fromJson(
                throwable.response()?.errorBody()!!.charStream().readText(),
                ErrorResponse::class.java
              )

              return Resource.Failure(
                FailureStatus.API_FAIL,
                throwable.code(),
                errorResponse.detail
              )
            }
            else -> {
              val jObjError = JSONObject(throwable.response()!!.errorBody()!!.string())
              val apiResponse = jObjError.toString()
              val response = Gson().fromJson(apiResponse, BaseResponse::class.java)
              Log.d(TAG, "safeApiCall: ${response.message}")
              return Resource.Failure(FailureStatus.API_FAIL,throwable.code(), response.message)

//              val apiResponse = apiCall.invoke()
//              val errorResponse = Gson().fromJson(
//                apiResponse.toString(),
//                BaseResponse::class.java
//              )
//              Log.d(TAG, "safeApiCall: ${apiResponse.toString()}")
              return if (throwable.response()?.errorBody()!!.charStream().readText().isEmpty()) {
                Resource.Failure(FailureStatus.API_FAIL, throwable.code())
              } else {
                try {
                  val errorResponse = Gson().fromJson(
                    throwable.response()?.errorBody()!!.charStream().readText(),
                    ErrorResponse::class.java
                  )
                  Log.d(TAG, "safeApiCall ${errorResponse?.detail}")
                  Resource.Failure(FailureStatus.API_FAIL, throwable.code(), errorResponse?.detail)
                } catch (ex: JsonSyntaxException) {
                  Resource.Failure(FailureStatus.API_FAIL, throwable.code())
                }
              }
            }
          }
        }

        is UnknownHostException -> {
          return Resource.Failure(FailureStatus.NO_INTERNET)
        }

        is ConnectException -> {
          return Resource.Failure(FailureStatus.NO_INTERNET)
        }

        else -> {
          return Resource.Failure(FailureStatus.OTHER)
        }
      }
    }
  }
}
package app.grand.tafwak.domain.auth.entity.request

import androidx.annotation.Keep
import androidx.databinding.ObservableField

@Keep
class LogInRequest {
var phone: String = ""
  var password: String = ""
  var device_token: String = ""
}

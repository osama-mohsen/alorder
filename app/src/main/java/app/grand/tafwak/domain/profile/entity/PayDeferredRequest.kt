package app.grand.tafwak.domain.profile.entity

import androidx.annotation.Keep
import androidx.databinding.ObservableField

@Keep
class PayDeferredRequest {
var order_id: Int = 0
  var amount: Double = 0.0
}

package app.grand.tafwak.domain.order


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose
import retrofit2.http.Query




@Keep
data class OrderDetailsResponse(
    @SerializedName("order")
    @Expose val order: Order = Order(),
    @SerializedName("products")
    @Expose val products: List<Product> = listOf(),
) : BaseResponse(){
    @Keep
    data class Order(
        @SerializedName("address")
        @Expose val address: Address = Address(),
        @SerializedName("address_id")
        @Expose val addressId: Int = 0,
        @SerializedName("company_id")
        @Expose val companyId: Int = 0,
        @SerializedName("created_at")
        @Expose val createdAt: String = "",
        @SerializedName("delivery_date")
        @Expose val deliveryDate: String = "",
        @SerializedName("id")
        @Expose val id: Int = 0,
        @SerializedName("order_status")
        @Expose val orderStatus: Int = 0,
        @SerializedName("order_status_text")
        @Expose val orderStatusText: String = "",
        @SerializedName("paid_amount")
        @Expose val paidAmount: Int = 0,
        @SerializedName("payment_method")
        @Expose val paymentMethod: String = "",
        @SerializedName("payment_method_text")
        @Expose val paymentMethodText: String = "",
        @SerializedName("payment_status")
        @Expose val paymentStatus: String = "",
        @SerializedName("payment_status_text")
        @Expose val paymentStatusText: String = "",
        @SerializedName("representative_id")
        @Expose val representativeId: Int = 0,
        @SerializedName("total")
        @Expose val total: Int = 0,
        @SerializedName("user_id")
        @Expose val userId: Int = 0,
        @SerializedName("uuid")
        @Expose val uuid: String = ""
    ) {
        @Keep
        data class Address(
            @SerializedName("address")
            @Expose val address: String = "",
            @SerializedName("city")
            @Expose val city: Int = 0,
            @SerializedName("created_at")
            @Expose val createdAt: String = "",
            @SerializedName("gov")
            @Expose val gov: Int = 0,
            @SerializedName("id")
            @Expose val id: Int = 0,
            @SerializedName("map_lat")
            @Expose val mapLat: String = "",
            @SerializedName("map_long")
            @Expose val mapLong: String = "",
            @SerializedName("name")
            @Expose val name: String = "",
            @SerializedName("notes")
            @Expose val notes: String = "",
            @SerializedName("phone")
            @Expose val phone: String = "",
            @SerializedName("user_id")
            @Expose val userId: Int = 0
        )
    }
}
package app.grand.tafwak.domain.home.use_case

import android.util.Log
import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.home.repository.HomeRepository
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.order.OrderDetailsResponse
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class HomeUseCase @Inject constructor(
  private val homeRepository: HomeRepository
) {

  fun homeService(): Flow<Resource<HomeResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.home()
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun updateToken(): Flow<Resource<BaseResponse>> = flow {
    Log.e("invoke", "invoke: starting")
//    emit(Resource.Loading)
    val result = homeRepository.updateToken()
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun orders(): Flow<Resource<OrderListResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.orders()
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun getOrderDetails(id: Int): Flow<Resource<OrderDetailsResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.getOrderDetails(id)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun categoryDetails(id: Int ,page:Int): Flow<Resource<CategoryDetailsResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.categoryDetails(id,page)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun products(categoryId: Int?,search: String?, companyId: Int?,featured: Int?, page: Int,showProgress:Boolean = true): Flow<Resource<ProductsResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    if(showProgress) emit(Resource.Loading)
    val result = homeRepository.products(categoryId,search,companyId,featured,page)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun companies(search: String): Flow<Resource<CompaniesResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.companies(search)
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun companiesCategories(companyId: Int): Flow<Resource<CompaniesCategoriesResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.companiesCategories(companyId)
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun submitCart(request: ArrayList<Cart>): Flow<Resource<BaseResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.submitCart(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun submitPassword(request: ConfirmPasswordRequest): Flow<Resource<BaseResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.submitPassword(request)
    emit(result)
  }.flowOn(Dispatchers.IO)



  fun sendComplain(request: ComplainRequest): Flow<Resource<BaseResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.sendComplain(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun confirmDeliver(request: Int): Flow<Resource<BaseResponse>> = flow {
    Log.e("invoke", "invoke: starting")
    emit(Resource.Loading)
    val result = homeRepository.confirmDeliver(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

}

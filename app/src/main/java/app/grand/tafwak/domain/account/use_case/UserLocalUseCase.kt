package app.grand.tafwak.domain.account.use_case

import app.grand.tafwak.domain.account.repository.AccountRepository
import app.grand.tafwak.domain.auth.entity.model.User
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserLocalUseCase @Inject constructor(private val accountRepository: AccountRepository) {
  suspend operator fun invoke(user: User) = accountRepository.saveUserToLocal(user)
  operator fun invoke(): User = accountRepository.getUserToLocal()
  suspend fun saveUserToken(value: String) =
    accountRepository.saveUserToken(value)

  fun clearUser() {
    accountRepository.clearUser()
  }

  fun getKey(key: String) : String{
    return accountRepository.getKeyFromLocal(key)
  }

  fun isLoggin(): Boolean =
    accountRepository.getIsLoggedIn()

}
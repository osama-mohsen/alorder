package app.grand.tafwak.domain.home.models

import android.util.Log
import androidx.annotation.Keep
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class CompanyCart(
  var id: Int = 0,
  var companyName: String = "",
  var companyImage: String = "",
  var total: Double = 0.0,
  var products: ArrayList<Product> = arrayListOf(),
){
  private val TAG = "CompanyCart"
  var willPay = ""
    get() = field
    set(value) {
      Log.d(TAG, ": $value")
      field = value
    }
}
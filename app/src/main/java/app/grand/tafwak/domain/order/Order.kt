package app.grand.tafwak.domain.order

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Order(@SerializedName("address_id")
              @Expose val addressId: Int = 0,
            @SerializedName("company_id")
              @Expose val companyId: Int = 0,
            @SerializedName("created_at")
              @Expose val createdAt: String = "",
            @SerializedName("delivery_date")
              @Expose val deliveryDate: String = "",
            @SerializedName("id")
              @Expose val id: Int = 0,
            @SerializedName("order_status")
              @Expose val orderStatus: Int = 0,
            @SerializedName("order_status_text")
              @Expose val orderStatusText: String = "",
            @SerializedName("paid_amount")
              @Expose val paidAmount: Int = 0,
            @SerializedName("payment_method")
              @Expose val paymentMethod: String = "",
            @SerializedName("payment_method_text")
              @Expose val paymentMethodText: String = "",
            @SerializedName("payment_status")
              @Expose val paymentStatus: String = "",
            @SerializedName("payment_status_text")
              @Expose val paymentStatusText: String = "",
            @SerializedName("representative_id")
              @Expose val representativeId: Int = 0,
            @SerializedName("total")
              @Expose val total: Int = 0,
            @SerializedName("user_id")
              @Expose val userId: Int = 0,
            @SerializedName("uuid")
              @Expose val uuid: String = "") : Serializable
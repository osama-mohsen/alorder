package app.grand.tafwak.domain.auth.use_case

import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.auth.enums.AuthFieldsValidation
import app.grand.tafwak.domain.auth.repository.AuthRepository
import app.grand.tafwak.domain.profile.entity.UpdatePassword
import app.grand.tafwak.domain.utils.*
import app.grand.tafwak.presentation.base.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class ChangePasswordUseCase @Inject constructor(
  private val authRepository: AuthRepository
) {

  operator fun invoke(request: ChangePasswordRequest): Flow<Resource<BaseResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.changePasswordNotAuthorized(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun updatePasswordAuthorize(request: ChangePasswordRequest): Flow<Resource<BaseResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.changePasswordAuthorized(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

  operator fun invoke(request: UpdatePassword): Flow<Resource<BaseResponse>> = flow {
    emit(Resource.Loading)
    val result = authRepository.updatePassword(request)
    emit(result)
  }.flowOn(Dispatchers.IO)

}
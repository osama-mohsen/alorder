package app.grand.tafwak.domain.utils

open class BaseResponse(
  val success: Boolean = false,
  val message: String = "",
)
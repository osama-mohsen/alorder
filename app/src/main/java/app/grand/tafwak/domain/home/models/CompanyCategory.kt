package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.structure.base_mvvm.R

data class CompanyCategory(
  @SerializedName("company_id")
  @Expose
  val id: Int = 0,
  @SerializedName("category_name")
  @Expose
  val name: String = "",
  @SerializedName("category_id")
  @Expose
  val categoryId: Int = 0,
)
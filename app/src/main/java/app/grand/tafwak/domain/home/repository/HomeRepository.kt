package app.grand.tafwak.domain.home.repository
import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.order.OrderDetailsResponse
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource

interface HomeRepository {
  suspend fun home(): Resource<HomeResponse>
  suspend fun updateToken(): Resource<BaseResponse>
  suspend fun orders(): Resource<OrderListResponse>
  suspend fun getOrderDetails(id: Int): Resource<OrderDetailsResponse>
  suspend fun companies(search: String): Resource<CompaniesResponse>
  suspend fun submitCart(request: ArrayList<Cart>): Resource<BaseResponse>
  suspend fun submitPassword(request: ConfirmPasswordRequest): Resource<BaseResponse>

  suspend fun confirmDeliver(request: Int): Resource<BaseResponse>
  suspend fun sendComplain(request: ComplainRequest): Resource<BaseResponse>

  suspend fun companiesCategories(request: Int): Resource<CompaniesCategoriesResponse>

  suspend fun categoryDetails(id: Int , page:Int): Resource<CategoryDetailsResponse>
  suspend fun products(categoryId: Int?,search: String?, companyId: Int?,featured: Int?, page: Int): Resource<ProductsResponse>
}
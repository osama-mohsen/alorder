package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose

@Keep
data class CategoryDetailsResponse(
    @SerializedName("category")
    @Expose val category: Category = Category(),
    @SerializedName("subCategories")
    @Expose val subCategories: List<Category> = listOf(),
    @SerializedName("companies")
    @Expose val companies: List<Company> = listOf(),
    @SerializedName("products")
    @Expose val products: List<Product> = listOf()
) : BaseResponse() {
}
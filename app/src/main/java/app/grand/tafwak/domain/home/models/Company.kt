package app.grand.tafwak.domain.home.models

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class Company(
  @SerializedName("id")
  @Expose val id: Int = 0,
  @SerializedName("image")
  @Expose val image: String = "",
  @SerializedName("name")
  @Expose val name: String = "",
  @SerializedName("status")
  @Expose val status: Int = 0
)
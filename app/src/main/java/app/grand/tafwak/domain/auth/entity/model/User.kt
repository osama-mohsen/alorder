package app.grand.tafwak.domain.auth.entity.model

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class User(

  @SerializedName("allow_deferred_payment")
  @Expose val allowDeferredPayment: Int = 0,
  @SerializedName("city_id")
  @Expose val cityId: Int = 0,
  @SerializedName("company_id")
  @Expose val companyId: Int = 0,
  @SerializedName("complete_profile")
  @Expose val completeProfile: Int = 0,
  @SerializedName("email")
  @Expose var email: String = "",
  @SerializedName("gov_id")
  @Expose val govId: Int = 0,
  @SerializedName("id")
  @Expose val id: Int = 0,
  @SerializedName("map_lat")
  @Expose val mapLat: String = "",
  @SerializedName("map_long")
  @Expose val mapLong: String = "",
  @SerializedName("name")
  @Expose var name: String = "",
  @SerializedName("phone")
  @Expose var phone: String = "",
  @SerializedName("role_id")
  @Expose val roleId: Int = 0,
  @SerializedName("status")
  @Expose val status: Int = 0,
  var userToken: String = "",
  @SerializedName("profile_img")
  @Expose var profileImg: String? = "",

)
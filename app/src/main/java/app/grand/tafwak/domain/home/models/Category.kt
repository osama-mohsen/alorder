package app.grand.tafwak.domain.home.models

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Keep
data class Category(
  @SerializedName("created_at")
  @Expose val createdAt: String = "",
  @SerializedName("id")
  @Expose var id: Int = 0,
  @SerializedName("image")
  @Expose val image: String = "",
  @SerializedName("name")
  @Expose var name: String = "",
  @SerializedName("parent_id")
  @Expose val parentId: Int = 0,
  @SerializedName("status")
  @Expose val status: Int = 0
)
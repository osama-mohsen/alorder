package app.grand.tafwak.domain.order

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.http.Query

@Keep
class SendComplainRequest() {
  var order_id: Int = 0
  var  type: Int = 0
  var content: String = ""
}
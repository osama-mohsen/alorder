package app.grand.tafwak.domain.profile.entity

import androidx.annotation.Keep
import androidx.databinding.ObservableField
import app.grand.tafwak.domain.utils.isValidEmail
import app.grand.tafwak.presentation.base.utils.Constants

@Keep
class AddAddress {
  var name: String = ""
  var address: String = "Address"
  var phone: String = ""
  var gov = 0
  var city = 0
  var govName: String = ""
  var cityName: String = ""
  var map_lat = 32.04848
  var map_long = 32.04848
  var notes: String = ""
}
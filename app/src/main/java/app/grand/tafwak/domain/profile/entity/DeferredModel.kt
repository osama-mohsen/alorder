package app.grand.tafwak.domain.profile.entity


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import java.io.Serializable

@Keep
data class DeferredModel(
    @SerializedName("order_id")
    @Expose val orderId: Int = 0,
    @SerializedName("order_total")
    @Expose val orderTotal: Int = 0,
    @SerializedName("paid_amount")
    @Expose val paidAmount: Int = 0,
    @SerializedName("remaining_amount")
    @Expose val remainingAmount: Int = 0
) : Serializable
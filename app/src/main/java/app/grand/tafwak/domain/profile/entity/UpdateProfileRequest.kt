package app.grand.tafwak.domain.profile.entity

import androidx.annotation.Keep
import androidx.databinding.ObservableField
import app.grand.tafwak.domain.utils.BaseRequest
import com.google.gson.annotations.Expose

@Keep
class UpdateProfileRequest : BaseRequest() {
  var name: String = ""
  var phone: String = ""
  var email: String = ""
}
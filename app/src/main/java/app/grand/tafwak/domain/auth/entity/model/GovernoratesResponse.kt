package app.grand.tafwak.domain.auth.entity.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose

@Keep
data class GovernoratesResponse(
    @SerializedName("governorates")
    @Expose val governorates: List<Governorate> = listOf(),
) : BaseResponse(){
    @Keep
    data class Governorate(
        @SerializedName("governorate_name_ar")
        @Expose val governorateNameAr: String = "",
        @SerializedName("governorate_name_en")
        @Expose val governorateNameEn: String = "",
        @SerializedName("id")
        @Expose val id: Int = 0
    )
}
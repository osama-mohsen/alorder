package app.grand.tafwak.domain.profile.entity

import androidx.annotation.Keep
import androidx.databinding.ObservableField
import app.grand.tafwak.domain.utils.isValidEmail
import app.grand.tafwak.presentation.base.utils.Constants

@Keep
class UpdatePassword {
  var password: String = ""
  var password2: String = ""
  var user_id = -1
}
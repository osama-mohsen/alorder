package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import androidx.room.Entity
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose

@Keep
data class ProductsResponse(
    @SerializedName("products")
    @Expose val products: List<Product> = listOf()
) : BaseResponse() {
}
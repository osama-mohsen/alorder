package app.grand.tafwak.domain.auth.use_case

import android.util.Log
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.LogInRequest
import app.grand.tafwak.domain.auth.repository.AuthRepository
import app.grand.tafwak.domain.utils.*
import app.grand.tafwak.presentation.base.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class LogInUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val userLocalUseCase: UserLocalUseCase
) {
  private val TAG = "LogInUseCase"
  operator fun invoke(
    request: LogInRequest
  ): Flow<Resource<UserResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.logIn(request)
    if (result is Resource.Success) {
      result.value.user.userToken = result.value.userToken
      Log.d(TAG, "token: ${result.value.user.userToken}")
      userLocalUseCase.invoke(result.value.user)
    }
    emit(result)
  }.flowOn(Dispatchers.IO)

}
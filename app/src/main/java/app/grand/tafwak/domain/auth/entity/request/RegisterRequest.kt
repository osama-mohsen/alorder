package app.grand.tafwak.domain.auth.entity.request

import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseRequest
import app.grand.tafwak.presentation.base.utils.Constants

@Keep
open class RegisterRequest(
  var name: String = "",
  var email: String = "",
  var password: String = "",
  var password2: String = "",
  var phone: String = "",
) : BaseRequest()

class RegisterValidationException(private val validationType: String) : Exception(validationType)

package app.grand.tafwak.domain.home.models

import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class Banner(
  @SerializedName("id")
  @Expose val id: Int = 0,
  @SerializedName("image")
  @Expose val image: String = "",
  @SerializedName("link")
  @Expose val link: String = "",
  @SerializedName("link_title")
  @Expose val linkTitle: Any = Any(),
  @SerializedName("status")
  @Expose val status: Int = 0,
  @SerializedName("type")
  @Expose val type: String = ""
)
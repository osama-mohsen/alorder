package app.grand.tafwak.domain.profile.use_case

import android.net.Uri
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.profile.entity.AvatarResponse
import app.grand.tafwak.domain.profile.entity.UpdateProfileRequest
import app.grand.tafwak.domain.profile.repository.ProfileRepository
import app.grand.tafwak.domain.utils.*
import app.grand.tafwak.presentation.base.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import javax.inject.Inject


class ProfileUseCase @Inject constructor(
  private val profileRepository: ProfileRepository,
  private val userLocalUseCase: UserLocalUseCase
) {

  operator fun invoke(): Flow<Resource<ArrayList<AvatarResponse>>> = flow {
    emit(Resource.Loading)
    val result = profileRepository.avatarList()
    emit(result)
  }.flowOn(Dispatchers.IO)

  operator fun invoke(
    request: UpdateProfileRequest
  ): Flow<Resource<BaseResponse>> = flow {
    emit(Resource.Loading)
    val result = profileRepository.updateProfile(request)
//    if (result is Resource.Success) {
//      userLocalUseCase.saveUserToken(result.value.userToken)
//    }
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun updateImage(
    uri: Uri
  ): Flow<Resource<UserResponse>> = flow {
    emit(Resource.Loading)
    val result = profileRepository.updateImage(uri)
    emit(result)
  }.flowOn(Dispatchers.IO)
}
package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity(
  tableName = "product",
)
@Keep
data class Product(
  @SerializedName("id")
  @Expose val id: Int = 0,

  @SerializedName("product_name")
  @Expose var product_name: String? = "",
  @SerializedName("name")
  @Expose var name: String = "",
  @PrimaryKey(autoGenerate = true)
  val roomId: Int? = null,
  @ColumnInfo(name = "quantity")
  var quantity: Int = 1,
  @SerializedName("quantity_amount")
  @ColumnInfo(name = "quantity_amount")
  var quantityAmount: String?,
  @SerializedName("quantity_type")
  @ColumnInfo(name = "quantity_type")
  var quantityType: String?,
  @SerializedName("company_name")
  @Expose val companyName: String = "",
  @SerializedName("company_image")
  @Expose val companyImage: String = "",
  @SerializedName("category_id")
  @Expose val categoryId: Int = 0,
  @SerializedName("company_id")
  @Expose val companyId: Int = 0,
  @SerializedName("image")
  @Expose val image: String = "",
  @SerializedName("status")
  @Expose val status: Int = 0,
  @SerializedName("price")
  @Expose val price: Double = 0.0,
  @SerializedName("is_favourite")
  @Expose val isFavourite: Int = 0
) {
}
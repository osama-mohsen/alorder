package app.grand.tafwak.domain.auth.entity.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose

@Keep
data class CitiesResponse(
    @SerializedName("cities")
    @Expose val cities: List<City> = listOf(),
) :BaseResponse() {
    @Keep
    data class City(
        @SerializedName("city_name_ar")
        @Expose val cityNameAr: String = "",
        @SerializedName("city_name_en")
        @Expose val cityNameEn: String = "",
        @SerializedName("governorate_id")
        @Expose val governorateId: Int = 0,
        @SerializedName("id")
        @Expose val id: Int = 0
    )
}
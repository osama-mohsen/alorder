package app.grand.tafwak.domain.auth.use_case

import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.RegisterRequest
import app.grand.tafwak.domain.auth.entity.request.RegisterValidationException
import app.grand.tafwak.domain.auth.enums.AuthFieldsValidation
import app.grand.tafwak.domain.auth.repository.AuthRepository
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class RegisterUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val userLocalUseCase: UserLocalUseCase
) {

  @Throws(RegisterValidationException::class)
  operator fun invoke(request: RegisterRequest): Flow<Resource<BaseResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.register(request)
    if (result is Resource.Success) {
//      userLocalUseCase.registerStep((request.register_steps + 1).toString())
    }
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun addAddress(request: AddAddress): Flow<Resource<UserResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.addAddress(request)
    if (result is Resource.Success) {
      result.value.user.userToken = userLocalUseCase.invoke().userToken
      userLocalUseCase.invoke(result.value.user)
    }
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun getGovernorates(): Flow<Resource<GovernoratesResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.getGovernorates()
    if (result is Resource.Success) {
//      userLocalUseCase.registerStep((request.register_steps + 1).toString())
    }
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun getDeferredPayments(): Flow<Resource<DeferredPaymentsResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.getDeferredPayments()
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun payDeferred(request: PayDeferredRequest): Flow<Resource<BaseResponse>> = flow {
    emit(Resource.Loading)
    val result = authRepository.payDeferred(request)
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun getCities(id: Int): Flow<Resource<CitiesResponse>> = flow {

    emit(Resource.Loading)
    val result = authRepository.getCities(id)
    if (result is Resource.Success) {
//      userLocalUseCase.registerStep((request.register_steps + 1).toString())
    }
    emit(result)
  }.flowOn(Dispatchers.IO)
}
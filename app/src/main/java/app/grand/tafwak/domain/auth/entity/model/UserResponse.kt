package app.grand.tafwak.domain.auth.entity.model

import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class UserResponse(
  @SerializedName("user")
  @Expose val user: User = User(),
  @SerializedName("userToken")
  @Expose val userToken: String = "",
  @SerializedName("profile_img")
  @Expose var profileImg: String? = "",
) : BaseResponse() {

}
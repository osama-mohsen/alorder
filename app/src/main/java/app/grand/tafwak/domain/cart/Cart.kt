package app.grand.tafwak.domain.cart

import androidx.annotation.Keep
import androidx.databinding.ObservableField
import app.grand.tafwak.domain.home.models.Product

@Keep
class Cart (
  var products: ArrayList<Product> = ArrayList(),
  var total: Double = 0.0,
  var paid_amount: String = "",
  var payment_method: Int = 1,
  var delivery_date: String = "",
  var company_id: Int = -1
)

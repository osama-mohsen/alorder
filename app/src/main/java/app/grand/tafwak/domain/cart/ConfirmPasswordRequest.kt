package app.grand.tafwak.domain.cart

import androidx.annotation.Keep
import androidx.databinding.ObservableField
import app.grand.tafwak.domain.home.models.Product

@Keep
class ConfirmPasswordRequest (
  var password: String = "",
)

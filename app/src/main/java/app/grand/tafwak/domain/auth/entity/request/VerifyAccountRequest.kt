package app.grand.tafwak.domain.auth.entity.request

import androidx.annotation.Keep

@Keep
data class VerifyAccountRequest(
  var type: String = "",
  var user_id: Int = 1,
  var phone: String = "",
  var code: String = ""
)

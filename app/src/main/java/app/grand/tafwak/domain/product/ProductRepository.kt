package app.grand.tafwak.domain.product
import app.grand.tafwak.domain.home.models.CategoryDetailsResponse
import app.grand.tafwak.domain.home.models.HomeResponse
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.models.ProductsResponse
import app.grand.tafwak.domain.utils.Resource

interface ProductRepository {
  suspend fun products(): Resource<List<Product>>
  suspend fun delete(id: Int)
  suspend fun products(categoryId: Int, companyId: Int, page: Int): Resource<ProductsResponse>
}
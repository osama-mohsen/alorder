package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose

@Keep
data class HomeResponse(
  @SerializedName("banners")
    @Expose val banners: ArrayList<Banner> = arrayListOf(),
  @SerializedName("categories")
    @Expose val categories: List<Category> = listOf(),
  @SerializedName("companies")
    @Expose val companies: List<Company> = listOf()
) : BaseResponse() {
}
package app.grand.tafwak.domain.profile.entity


import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DeferredPaymentsResponse(
  @SerializedName("remainingAmounts")
  val remainigAmounts: ArrayList<DeferredModel> = ArrayList(),
  @SerializedName("totalRemaining")
  val totalRemaining: Int = 0,
  @SerializedName("supportNumber")
  val supportNumber: String = "",
) : BaseResponse(), Serializable
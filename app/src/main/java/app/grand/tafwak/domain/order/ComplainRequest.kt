package app.grand.tafwak.domain.order

import androidx.annotation.Keep
import androidx.databinding.ObservableField

@Keep
class ComplainRequest {
  var order_id: Int = -1
  var type: Int = 1
  var content: String = ""
}

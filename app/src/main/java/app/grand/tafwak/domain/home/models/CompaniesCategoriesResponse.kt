package app.grand.tafwak.domain.home.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.google.gson.annotations.Expose

@Keep
data class CompaniesCategoriesResponse(
    @SerializedName("categories")
    @Expose val categories: List<CompanyCategory> = ArrayList()
) : BaseResponse() {
}
package app.grand.tafwak.domain.account.repository

import app.grand.tafwak.domain.account.entity.request.SendFirebaseTokenRequest
import app.grand.tafwak.domain.auth.entity.model.User
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import kotlinx.coroutines.flow.Flow

interface AccountRepository {


  suspend fun isLoggedIn(isLoggedIn: Boolean)

  fun getIsLoggedIn(): Boolean

  fun saveUserToLocal(user: User)
  fun getKeyFromLocal(key: String): String

  fun getUserToLocal(): User

  suspend fun saveUserToken(userToken: String)

  suspend fun getUserToken(): Flow<String>

  suspend fun saveCountryId(country_id: String)

  suspend fun getCountryId(): Flow<String>

  fun clearPreferences()

  fun saveKeyToLocal(key: String, value: String)
  fun clearUser()


}
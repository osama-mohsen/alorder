package app.grand.tafwak.domain.auth.repository

import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.ForgetPasswordResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.profile.entity.UpdatePassword
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource

interface AuthRepository {

  suspend fun logIn(request: LogInRequest): Resource<UserResponse>
  suspend fun getGovernorates(): Resource<GovernoratesResponse>
  suspend fun getDeferredPayments(): Resource<DeferredPaymentsResponse>
  suspend fun payDeferred(request:PayDeferredRequest): Resource<BaseResponse>

  suspend fun getCities(id: Int): Resource<CitiesResponse>

  suspend fun addAddress(request: AddAddress): Resource<UserResponse>
  suspend fun changePasswordNotAuthorized(request: ChangePasswordRequest): Resource<BaseResponse>
  suspend fun changePasswordAuthorized(request: ChangePasswordRequest): Resource<BaseResponse>
  suspend fun updatePassword(request: UpdatePassword): Resource<BaseResponse>
  suspend fun forgetPassword(request: ForgetPasswordRequest): Resource<ForgetPasswordResponse>
  suspend fun register(request: RegisterRequest): Resource<BaseResponse>
  suspend fun verifyAccount(request: VerifyAccountRequest): Resource<BaseResponse>
}
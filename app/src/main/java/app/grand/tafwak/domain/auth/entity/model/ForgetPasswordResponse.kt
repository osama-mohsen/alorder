package app.grand.tafwak.domain.auth.entity.model

import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Keep
data class ForgetPasswordResponse(
  @SerializedName("user_id")
  @Expose var userId: Int = -1,
): BaseResponse(){
}
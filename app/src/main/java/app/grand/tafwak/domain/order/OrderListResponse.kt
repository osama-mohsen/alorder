package app.grand.tafwak.domain.order


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import app.grand.tafwak.domain.utils.BaseResponse
import com.google.gson.annotations.Expose
import java.io.Serializable

@Keep
data class OrderListResponse(
    @SerializedName("orders")
    @Expose val orders: List<Order> = listOf(),
): BaseResponse() , Serializable
package app.grand.tafwak.domain.profile.repository

import android.net.Uri
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.profile.entity.AvatarResponse
import app.grand.tafwak.domain.profile.entity.UpdateProfileRequest
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import okhttp3.MultipartBody

interface ProfileRepository {
  suspend fun updateProfile(request: UpdateProfileRequest): Resource<BaseResponse>
  suspend fun updateImage(request: Uri): Resource<UserResponse>
  suspend fun avatarList(): Resource<ArrayList<AvatarResponse>>
}
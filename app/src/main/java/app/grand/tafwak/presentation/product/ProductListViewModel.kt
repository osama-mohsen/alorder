package app.grand.tafwak.presentation.product

import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.section_details.adapters.ProductsAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase,
  val productLocalRepository: ProductLocalRepository
) : BaseViewModel() {

  var search: String? = null

  @Bindable
  var page: Int = 0

  @Bindable
  var allowCallService = true
  @Bindable
  var last = false
  var id: Int? = null
  var companyId: Int? = null


  @Bindable
  @Inject
  lateinit var productsAdapter: ProductsAdapter

  val _responseProductService =
    MutableStateFlow<Resource<ProductsResponse>>(Resource.Default)
  val responseProducts = _responseProductService


  val _responseCompanyCategories =
    MutableStateFlow<Resource<CompaniesCategoriesResponse>>(Resource.Default)
  val responseCompanyCategories = _responseCompanyCategories



  val btnCartPrice = ObservableBoolean(false)
  val cartCount = ObservableField("")
  val cartTotal = ObservableField("")

  private val TAG = "SectionDetailsViewModel"

  var total = 0.0

  init {
    Log.d(TAG, "HERERERERER")
    viewModelScope.launch {
      val productsCount = productLocalRepository.getProductsCount()
      productsCount.collect {
        btnCartPrice.set(it > 0)
        cartCount.set(it.toString())
      }
    }
    viewModelScope.launch {
      val productsTotal = productLocalRepository.products()
      productsTotal.collect {
        total = 0.0
        it.forEach { productItem ->
          total += productItem.quantity * productItem.price
        }
        Log.d(TAG, ": $total")
        cartTotal.set(total.toString())
      }
    }

  }


  fun setData(response: ProductsResponse) {
    last = (response.products.size < Constants.PAGINATION_LIMIT)
    allowCallService = true

    when (page) {
      1 -> {
        productsAdapter.insertData(response.products)
        notifyPropertyChanged(BR.productsAdapter)
      }
      else -> productsAdapter.insertNewData(response.products)
    }
    Log.d(TAG, "setData: ${productsAdapter.differ.currentList.size}")
//    notifyPropertyChanged(BR.productsAdapter)
    notifyPropertyChanged(BR.last)
    notifyPropertyChanged(BR.page)
    notifyPropertyChanged(BR.allowCallService)
  }

  fun callService() {
    if (!last && allowCallService) {
//      callingService = true
//      notifyPropertyChanged(BR.callingService)
      page++
      if (page > 1) {
        notifyPropertyChanged(BR.page)
        notifyPropertyChanged(BR.allowCallService)
        notifyPropertyChanged(BR.last)
      }
      allowCallService = false
      homeUseCase.products(id,search, companyId,null, page,page == 1)
        .onEach {
          responseProducts.value = it
        }
        .launchIn(viewModelScope)
    }
  }

  fun getCompanyProducts(){
    homeUseCase.companiesCategories(companyId!!)
      .onEach {
        responseCompanyCategories.value = it
      }
      .launchIn(viewModelScope)
  }

  fun resetData() {
    page = 0
    allowCallService = true
    last = false
  }

  fun setCategoriesCompanies(companiesCategoriesResponse: CompaniesCategoriesResponse) {
    Log.d(TAG, "setCategoriesCompanies: ${companiesCategoriesResponse.categories.size}")
  }


}
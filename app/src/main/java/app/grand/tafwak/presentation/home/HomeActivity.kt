package app.grand.tafwak.presentation.home

import android.content.Context
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.*
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseActivity
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding>() {
  private lateinit var appBarConfiguration: AppBarConfiguration
  private lateinit var nav: NavController

  override
  fun getLayoutId() = R.layout.activity_home

  override
  fun setUpBottomNavigation() {
    setUpBottomNavigationWithGraphs()
  }


  override fun setUpViews() {
    super.setUpViews()
    Log.d(TAG, "setUpViews: ")
//    binding.toolbar.setNavigationOnClickListener {
//      Log.d(TAG, "setUpViews: HERE")
//    }

  }



  private fun setUpBottomNavigationWithGraphs() {
    val navHostFragment =
      supportFragmentManager.findFragmentById(R.id.fragment_host_container) as NavHostFragment
    nav = navHostFragment.findNavController()
    appBarConfiguration = AppBarConfiguration(
      setOf(
        R.id.home_fragment,
        R.id.myOrdersFragment,
        R.id.accountFragment,
        R.id.notificationsFragment,
      ),
    )
    setSupportActionBar(binding.toolbar)
    setupActionBarWithNavController(nav, appBarConfiguration)
    binding.bottomNavigationView.setupWithNavController(nav)
    navChangeListener()
  }

  private  val TAG = "HomeActivity"
  private fun navChangeListener() {


    nav.addOnDestinationChangedListener { _, destination, _ ->

      binding.toolbar.visibility = View.VISIBLE
//      val view = binding.toolbar.menu[0].actionView
      if(destination.id == R.id.sectionDetailsFragment){
        binding.toolbar.menu[0].setIcon(R.drawable.ic_search)
      }else if(destination.id == R.id.home_fragment)
        binding.toolbar.menu[0].setIcon(R.drawable.ic_notification_icon)
      else {
        binding.toolbar.menu[0].icon = null
      }


      if (destination.id == R.id.home_fragment
        || destination.id == R.id.myOrdersFragment
        || destination.id == R.id.accountFragment
        || destination.id == R.id.searchFragment
        || destination.id == R.id.complainDialog
      ) {
        binding.bottomNavigationView.visibility = View.VISIBLE
        binding.toolbar.visibility = View.GONE
      } else {
        binding.bottomNavigationView.visibility = View.GONE
        binding.toolbar.visibility = View.VISIBLE
      }
    }
  }


  override fun onSupportNavigateUp(): Boolean {
    return nav.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.top_app_bar, menu)
    binding.toolbar.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener,
      Toolbar.OnMenuItemClickListener {
      override fun onMenuItemClick(p0: MenuItem?): Boolean {
        nav.navigate(R.id.to_search)
        return true
      }

    })
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return item.onNavDestinationSelected(nav) || super.onOptionsItemSelected(item)
  }

}
package app.grand.tafwak.presentation.product

import android.util.Log
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.section_details.viewModels.SectionDetailsViewModel
import com.structure.base_mvvm.databinding.FragmentProductListBinding
import com.structure.base_mvvm.databinding.FragmentSectionDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ProductListFragment : BaseFragment<FragmentProductListBinding>() {

  val viewModel: ProductListViewModel by viewModels()
  val args: ProductListFragmentArgs by navArgs()

  override
  fun getLayoutId() = R.layout.fragment_product_list

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.companyId = args.companyId
    when{
      viewModel.companyId!! > 0 -> viewModel.getCompanyProducts()
      else -> viewModel.callService()
    }
  }

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.responseProducts.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setData(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.responseCompanyCategories.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setCategoriesCompanies(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

    Log.d(TAG, "setupObservers: worked in product")
    binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        Log.d(TAG, "onScrollStateChanged: START")
        if (!recyclerView.canScrollVertically(1)) {
          Log.d(TAG, "onScrollStateChanged: CALLLLLLL")
          viewModel.callService()
        }
      }
    })

  }
  private  val TAG = "ProductListFragment"
}
package app.grand.tafwak.presentation.favorite.viewModels

import androidx.databinding.Bindable
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.favorite.adpater.FavoriteAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase
) : BaseViewModel() {

  @Bindable
  @Inject
  lateinit var adapter: FavoriteAdapter

  init {
    setupProduct()
  }

  fun setupProduct() {
    val list = ArrayList<Product>()
    adapter.differ.submitList(list)
//    notifyPropertyChanged(BR.CategoriesAdapter)
  }

}
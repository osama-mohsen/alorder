package app.grand.tafwak.presentation.profile.changePassword

import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.request.ChangePasswordRequest
import app.grand.tafwak.domain.auth.use_case.ChangePasswordUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
  private val changePasswordUseCase: ChangePasswordUseCase,
  val userLocalUseCase: UserLocalUseCase
) :
  BaseViewModel() {
  var from: String = Constants.HOME
  var request = ChangePasswordRequest()
  private val _changePasswordResponse =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val changePasswordResponse = _changePasswordResponse

  var isLogged: Boolean = false

  init {
    request.user_id = userLocalUseCase.invoke().id
  }

  fun changePassword(v: View) {
    if (request.password.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_password));
      return
    }
    if (request.password.trim().length < 6) {
      v.context.showError(v.context.getString(R.string.password_number_at_least_six_character));
      return
    }
    if (request.password2.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_confirm_password));
      return
    }
    if (request.password.trim() != request.password.trim()) {
      v.context.showError(v.context.getString(R.string.both_passwords_must_be_same));
      return
    }
    when(from){
      Constants.HOME -> {
        changePasswordUseCase.updatePasswordAuthorize(request)
          .onEach { result ->
            _changePasswordResponse.value = result
          }
          .launchIn(viewModelScope)
      }
      else -> {
        changePasswordUseCase(request)
          .onEach { result ->
            _changePasswordResponse.value = result
          }
          .launchIn(viewModelScope)
      }
    }
  }


  fun logOut() {
    userLocalUseCase.clearUser()
  }

}
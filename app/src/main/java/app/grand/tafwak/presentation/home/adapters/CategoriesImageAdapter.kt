package app.grand.tafwak.presentation.home.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.domain.home.models.Category
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import app.grand.tafwak.presentation.home.viewModels.ItemCategoryImageViewModel
import com.structure.base_mvvm.databinding.ItemCategoryImageBinding

class CategoriesImageAdapter : RecyclerView.Adapter<CategoriesImageAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<Category> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<Category>() {
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
      return oldItem == newItem
    }
  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category_image, parent, false)
    Log.d(TAG, "onCreateViewHolder: ")
    return ViewHolder(view)
  }

  private  val TAG = "CategoriesImageAdapter"
  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    Log.d(TAG, "onBindViewHolder: ${data.name}")
    val itemViewModel = ItemCategoryImageViewModel(data)
//    itemViewModel.clickEvent.observeForever {
//      clickEvent.value = data
//    }

    holder.setViewModel(itemViewModel)

  }

  override fun getItemCount(): Int {
    Log.d(TAG, "getItemCount: ${differ.currentList.size}")
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    private lateinit var itemLayoutBinding: ItemCategoryImageBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemCategoryImageViewModel) {
      Log.d(TAG, "setViewModel: ")
      itemLayoutBinding.itemViewModels = itemViewModel
    }
  }

}
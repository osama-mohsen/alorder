package app.grand.tafwak.presentation.splash

import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.general.use_case.GeneralUseCases
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.extensions.openActivityAndClearStack
import app.grand.tafwak.presentation.home.HomeActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val userLocalUseCase: UserLocalUseCase) :
  BaseViewModel() {

  fun submit(v: View) {
    when(userLocalUseCase.isLoggin()){
      true -> {
        when(isCompleteProfile()){
          true -> v.findFragment<SplashFragment>().openActivityAndClearStack(HomeActivity::class.java)
          else ->  v.findFragment<SplashFragment>().navigateSafe(SplashFragmentDirections.actionSplashFragmentToCompanyProfileFragment())
        }
      }
      else -> {
        v.findFragment<SplashFragment>().navigateSafe(
          SplashFragmentDirections.actionSplashFragmentToLogInFragment()
        )
      }
    }
  }

  private fun isCompleteProfile(): Boolean{
    val user = userLocalUseCase.invoke()
    return user.completeProfile == 1
  }
}
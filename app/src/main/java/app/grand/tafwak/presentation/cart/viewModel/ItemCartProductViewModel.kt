package app.grand.tafwak.presentation.cart.viewModel

import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.presentation.base.BaseViewModel
import com.structure.base_mvvm.R

class ItemCartProductViewModel(
  val product: Product,
  val position: Int,
  val productLocalRepository: ProductLocalRepository,
) :
  BaseViewModel() {

  val showDetails = ObservableBoolean(false)

  var willPay = "0"

//  val counter = ObservableInt(1)

  init {
  }

  private val TAG = "ItemCartCompanyViewMode"
  fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
    Log.d(TAG, "onSplitTypeChanged: ")
    when (id) {
      R.id.radio_cash -> Log.d(TAG, "onSplitTypeChanged: CASH")
      else -> Log.d(TAG, "onSplitTypeChanged: DEBIT")
    }
  }

//  fun add() {
//    counter.set(counter.get() + 1)
//  }
//
//  fun minus() {
//    if (counter.get() > 1)
//      counter.set(counter.get() - 1)
//  }
}
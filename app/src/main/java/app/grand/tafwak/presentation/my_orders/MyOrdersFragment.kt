package app.grand.tafwak.presentation.my_orders

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.handleApiError
import app.grand.tafwak.presentation.base.extensions.hideKeyboard
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.my_orders.viewModels.MyOrdersViewModel
import com.structure.base_mvvm.databinding.FragmentMyOrdersBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MyOrdersFragment : BaseFragment<FragmentMyOrdersBinding>() {

  private val viewModel: MyOrdersViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_my_orders

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    binding.toolbar.navigationIcon = null
  }

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.homeResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setData(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
  }

  override fun onResume() {
    super.onResume()
    viewModel.callService()
  }


}
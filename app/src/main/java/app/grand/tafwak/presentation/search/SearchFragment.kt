package app.grand.tafwak.presentation.search

import android.util.Log
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import com.structure.base_mvvm.databinding.FragmentHomeBinding
import app.grand.tafwak.presentation.home.viewModels.HomeViewModel
import app.grand.tafwak.presentation.product.ProductListViewModel
import app.grand.tafwak.presentation.search.viewModels.SearchViewModel
import com.structure.base_mvvm.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>() {

  private val viewModel: ProductListViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_search

  private  val TAG = "SearchFragment"
  override
  fun setBindingVariables() {
    binding.viewModel = viewModel

    binding.edtSearch.setOnEditorActionListener(TextView.OnEditorActionListener { textView, i, keyEvent ->
      Log.d(TAG, "setBindingVariables: HERE")
      if (i == EditorInfo.IME_ACTION_SEARCH && textView.text.trim().isNotEmpty()) {
        viewModel.search = textView.text.toString()
        viewModel.resetData()
        viewModel.callService()
        return@OnEditorActionListener true
      }
      false
    })
  }

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.responseProducts.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setData(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

    Log.d(TAG, "setupObservers: worked in product")
//    binding.nestedScrollView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//        super.onScrollStateChanged(recyclerView, newState)
//        Log.d(TAG, "onScrollStateChanged: START")
//        if (!recyclerView.canScrollVertically(1)) {
//          Log.d(TAG, "onScrollStateChanged: CALLLLLLL")
//          viewModel.callService()
//        }
//      }
//    })


    binding.nestedScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
      if (v.getChildAt(v.childCount - 1) != null) {
        if (scrollY >= v.getChildAt(v.childCount - 1)
            .measuredHeight - v.measuredHeight &&
          scrollY > oldScrollY
        ) {
          viewModel.callService()
          //code to fetch more data for endless scrolling
        }
      }
    } as NestedScrollView.OnScrollChangeListener)

  }

}
package app.grand.tafwak.presentation.account.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.presentation.account.AccountViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.PhoneDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhoneDialog : BottomSheetDialogFragment() {
  private val viewModel: AccountViewModel by viewModels()
  lateinit var binding: PhoneDialogBinding
  val args: PhoneDialogArgs by navArgs()
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    binding = DataBindingUtil.inflate(inflater, R.layout.phone_dialog, container, false)
    viewModel.phone = args.phone
    binding.viewModel = viewModel
    return binding.root
  }

  override fun getTheme(): Int {
    return R.style.CustomBottomSheetDialogTheme;
  }

  override fun onDestroy() {
    super.onDestroy()
  }
}
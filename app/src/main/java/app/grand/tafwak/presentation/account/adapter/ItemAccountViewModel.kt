package app.grand.tafwak.presentation.account.adapter

import android.accounts.Account
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import app.grand.tafwak.core.extenstions.showInfo
import app.grand.tafwak.presentation.account.AccountFragment
import app.grand.tafwak.presentation.account.AccountFragmentDirections
import app.grand.tafwak.presentation.account.AccountItem
import app.grand.tafwak.presentation.auth.AuthActivity
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.R
import java.lang.Exception
import java.net.URLEncoder

class ItemAccountViewModel constructor(val accountItem: AccountItem) : BaseViewModel() {
  fun submit(v: View) {
    if (accountItem.title !== v.resources.getString(R.string.log_out)) {
      when (accountItem.title) {
        v.resources.getString(R.string.payment_account) -> {
          if (v.findFragment<AccountFragment>().viewModel.accountAdapter.response.remainigAmounts.size == 0)
            v.context.showInfo(v.context.getString(R.string.you_dont_have_any_deferred))
          else
            v.findFragment<AccountFragment>()
              .navigateSafe(AccountFragmentDirections.actionAccountFragmentToDeferredFragment(v.findFragment<AccountFragment>().viewModel.accountAdapter.response))
        }
        v.resources.getString(R.string.support) ->{
          v.findFragment<AccountFragment>().navigateSafe(AccountFragmentDirections.actionAccountFragmentToPhoneDialog(accountItem.body))
//          var url = "https://api.whatsapp.com/send?phone=${accountItem.body}"
//          val i = Intent(Intent.ACTION_VIEW)
////          url += "&text=" + URLEncoder.encode(
////            titleWhatsapp + "\n" + description,
////            "UTF-8"
////          )
//          try {
//            i.setPackage("com.whatsapp")
//            i.data = Uri.parse(url)
//            v.context.startActivity(i)
//          } catch (e: Exception) {
//            try {
//              i.setPackage("com.whatsapp.w4b")
//              i.data = Uri.parse(url)
//              v.context.startActivity(i)
//            } catch (exception: Exception) {
//              v.context.showInfo(v.context.getString(R.string.please_install_whatsapp_on_your_phone));
//            }
//          }
        }
        else -> v.findNavController().navigate(accountItem.action!!)
      }
    } else {
      v.findFragment<AccountFragment>().viewModel.logOut()
      v.findFragment<AccountFragment>().openActivityAndClearStack(AuthActivity::class.java)
    }
  }
}
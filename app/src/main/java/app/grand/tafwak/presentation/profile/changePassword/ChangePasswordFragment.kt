package app.grand.tafwak.presentation.profile.changePassword

import android.util.Log
import android.window.SplashScreen
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.auth.AuthActivity
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.backToPreviousScreen
import app.grand.tafwak.presentation.base.extensions.handleApiError
import app.grand.tafwak.presentation.base.extensions.hideKeyboard
import app.grand.tafwak.presentation.base.extensions.openActivityAndClearStack
import app.grand.tafwak.presentation.base.utils.showMessage
import app.grand.tafwak.presentation.base.utils.showSuccessAlert
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentUpdatePasswordBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ChangePasswordFragment : BaseFragment<FragmentUpdatePasswordBinding>() {

  val args: ChangePasswordFragmentArgs by navArgs()

  private val viewModel: ChangePasswordViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_update_password

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.from = args.from
    viewModel.request.user_id = args.userId
//    viewModel.request.user_id = args.userId
  }

  private  val TAG = "ChangePasswordFragment"
  override
  fun setupObservers() {
    Log.d(TAG, "setupObservers: CHANGe PASSWORD")
    viewModel.clickEvent.observeForever { backToPreviousScreen() }
    lifecycleScope.launchWhenResumed {
      viewModel.changePasswordResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            showMessage(requireActivity(), it.value.message)
            viewModel.logOut()
            openActivityAndClearStack(AuthActivity::class.java)
//            backToPreviousScreen()
          }
          is Resource.Failure -> {
            hideLoading()
//            handleApiError(it)
          }
          Resource.Default -> {
          }
        }
      }
    }
  }


}
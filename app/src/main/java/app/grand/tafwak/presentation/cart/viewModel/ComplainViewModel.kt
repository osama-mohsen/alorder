package app.grand.tafwak.presentation.cart.viewModel

import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.Companies
import app.grand.tafwak.domain.home.models.HomeResponse
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.order.ComplainRequest
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.cart.CartFragment
import app.grand.tafwak.presentation.home.HomeFragmentDirections
import app.grand.tafwak.presentation.home.adapters.CategoriesImageAdapter
import app.grand.tafwak.presentation.home.adapters.HomeSliderAdapter
import app.grand.tafwak.presentation.home.adapters.CompaniesAdapter
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ComplainViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase,
  private val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {
  val progress = ObservableBoolean(false)

  val request=  ComplainRequest()

  val response =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val user = userLocalUseCase.invoke()
  init {
  }

  fun submit(v: View) {
    if(request.content.trim().isEmpty()){
      v.context.showError(v.context.getString(R.string.please_add_your_message));
    }
    homeUseCase.sendComplain(request)
      .onEach { result ->
        response.value = result
      }
      .launchIn(viewModelScope)

  }
  private  val TAG = "ComplainViewModel"
  fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
    Log.d(TAG, "onSplitTypeChanged: ")
    when (id) {
      R.id.radio_wrong_in_product -> {
        request.type = 1
      }
      else -> {
        Log.d(TAG, "onSplitTypeChanged: DEBIT")
        request.type = 2
      }
    }
  }


}
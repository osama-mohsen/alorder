package app.grand.tafwak.presentation.my_orders.viewModels

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.order.Order
import app.grand.tafwak.domain.order.OrderDetailsResponse
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.cart.adapters.CartProductAdapter
import app.grand.tafwak.presentation.my_orders.adapters.MyOrdersAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class MyOrderDetailsViewModel @Inject constructor(
  val productLocalRepository: ProductLocalRepository,
  private val homeUseCase: HomeUseCase
) : BaseViewModel() {

  private var id: Int = -1
  val order = ObservableField(OrderDetailsResponse())

  @Bindable
  var adapter: CartProductAdapter = CartProductAdapter(productLocalRepository)

  private val _homeResponse =
    MutableStateFlow<Resource<OrderDetailsResponse>>(Resource.Default)
  val homeResponse = _homeResponse

  val confirm_order =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)


  fun callService(id: Int) {
    this.id = id
    homeUseCase.getOrderDetails(id)
      .onEach {
        _homeResponse.value = it
      }.launchIn(viewModelScope)
  }

  fun setData(order: OrderDetailsResponse){
    this.order.set(order)
    order.products.forEach {
      it.name = it.product_name.toString()
    }
    adapter.differ.submitList(order.products)
    notifyPropertyChanged(BR.adapter)
  }

  fun confirmOrder(v: View){
    homeUseCase.confirmDeliver(id)
      .onEach {
        confirm_order.value = it
      }
      .launchIn(viewModelScope)
  }

}
package app.grand.tafwak.presentation.account

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.domain.utils.Resource
import codes.grand.pretty_pop_up.PrettyPopUpHelper
import app.grand.tafwak.presentation.auth.AuthActivity
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.base.utils.Constants
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentAccountBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class AccountFragment : BaseFragment<FragmentAccountBinding>() {

  val viewModel: AccountViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_account

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    setupAccount()
  }

  override
  fun setupObservers() {
//    viewModelTeacher.clickEvent.observe(this) { action ->
//      when (action) {
//        Constants.PROFILE -> {
//          openProfileScreen()
//        }
//
//        else -> showLogOutPopUp()
//      }
//    }
//    lifecycleScope.launchWhenResumed {
//      viewModelTeacher.logOutResponse.collect {
//        when (it) {
//          Resource.Loading -> {
//            hideKeyboard()
//            showLoading()
//          }
//          is Resource.Success -> {
//            hideLoading()
//            viewModelTeacher.clearStorage()
//            openLogInScreen()
//          }
//          is Resource.Failure -> {
//            hideLoading()
//            handleApiError(it)
//          }
//        }
//      }
//    }
  }

  fun setupAccount() {
    val list = ArrayList<AccountItem>(6).also { list ->
      list.add(
        AccountItem(
          getMyDrawable(R.drawable.ic_profile),
          getString(R.string.profile_contact),
          "",
          AccountFragmentDirections.actionAccountFragmentToProfileFragment()
        )
      )
//      list.add(
//        AccountItem(
//          getMyDrawable(R.drawable.ic_account_favorite),
//          getString(R.string.favorite),
//          getString(R.string.favorite_body),
//          AccountFragmentDirections.actionAccountFragmentToFavoriteFragment()
//        )
//      )
      list.add(
        AccountItem(
          getMyDrawable(R.drawable.ic_lock),
          getString(R.string.password_account),
          "",
          AccountFragmentDirections.actionAccountFragmentToUpdatePasswordFragment(Constants.HOME)
        )
      )
//      list.add(
//        AccountItem(
//          getMyDrawable(R.drawable.ic_account_home),
//          getString(R.string.tv_building_info),
//          getString(R.string.tv_building_info_change),
//          AccountFragmentDirections.actionAccountFragmentToUpdateEnterpriseFragment()
//        )
//      )
//      list.add(
//        AccountItem(
//          getMyDrawable(R.drawable.ic_credit_card),
//          getString(R.string.payment_account),
//          "0 ج.م",
//          AccountFragmentDirections.actionAccountFragmentToDeferredFragment(viewModel.accountAdapter.response)
//        )
//      )
      list.add(
        AccountItem(
          getMyDrawable(R.drawable.ic_support),
          getString(R.string.support),
          "",
          AccountFragmentDirections.actionAccountFragmentToProfileFragment()
        )
      )
      list.add(
        AccountItem(
          getMyDrawable(R.drawable.ic_logout),
          getString(R.string.log_out),
          ""
        )
      )
    }
    viewModel.accountAdapter.differ.submitList(list)
    viewModel.notifyPropertyChanged(BR.accountAdapter)

    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
//            viewModel.accountAdapter.updateDeferred(it.value)
            viewModel.accountAdapter.updateSupport(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

  }
  private fun openLogInScreen() {
    requireActivity().openActivityAndClearStack(AuthActivity::class.java)
  }

  private fun openProfileScreen() {
    navigateSafe(AccountFragmentDirections.actionAccountFragmentToProfileFragment())
  }

  override fun onResume() {
    super.onResume()
    viewModel.name = viewModel.userLocalUseCase.invoke().name
    viewModel.loadLocalImage()
    viewModel.getDeferredPayments()
    viewModel.notifyChange()
  }
}
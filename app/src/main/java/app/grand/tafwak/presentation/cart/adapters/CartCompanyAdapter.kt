package app.grand.tafwak.presentation.cart.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.CompanyCart
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import app.grand.tafwak.presentation.cart.viewModel.ItemCartCompanyViewModel
import com.structure.base_mvvm.databinding.ItemCartCompanyBinding

class CartCompanyAdapter constructor(val productLocalRepository: ProductLocalRepository,val userLocalUseCase: UserLocalUseCase) : RecyclerView.Adapter<CartCompanyAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<CompanyCart> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<CompanyCart>() {
    override fun areItemsTheSame(oldItem: CompanyCart, newItem: CompanyCart): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CompanyCart, newItem: CompanyCart): Boolean {
      return oldItem == newItem
    }
  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_company, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemCartCompanyViewModel(data,position,productLocalRepository,userLocalUseCase)
    holder.setViewModel(itemViewModel)

  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

    fun remove(position: Int) {
      val list = ArrayList<CompanyCart>(differ.currentList)
      list.removeAt(position)
      notifyItemRemoved(position)
    }

  fun delete(position: Int) {
    val list = ArrayList(differ.currentList)
    if(position < list.size)
      list.removeAt(position)
    submitList(list)
  }


  fun submitList(list: ArrayList<CompanyCart>){
    if(differ.currentList.size > 0)
    differ.submitList(null)
    differ.submitList(list)
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemCartCompanyBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemCartCompanyViewModel) {
      itemLayoutBinding.itemViewModels = itemViewModel
    }
  }

}
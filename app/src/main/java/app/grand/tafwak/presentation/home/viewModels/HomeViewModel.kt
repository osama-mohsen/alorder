package app.grand.tafwak.presentation.home.viewModels

import android.util.Log
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.Companies
import app.grand.tafwak.domain.home.models.HomeResponse
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.home.HomeFragmentDirections
import app.grand.tafwak.presentation.home.adapters.CategoriesImageAdapter
import app.grand.tafwak.presentation.home.adapters.HomeSliderAdapter
import app.grand.tafwak.presentation.home.adapters.CompaniesAdapter
import com.structure.base_mvvm.BR
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase,
  private val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {

  @Bindable
  val adapter: CompaniesAdapter = CompaniesAdapter()

  @Bindable
  val categoriesImageAdapter: CategoriesImageAdapter = CategoriesImageAdapter()

  @Bindable
  val homeSliderAdapter: HomeSliderAdapter = HomeSliderAdapter()

  val homeData = ObservableField(HomeResponse())

  private val _homeResponse =
    MutableStateFlow<Resource<HomeResponse>>(Resource.Default)
  val homeResponse = _homeResponse
  val user = userLocalUseCase.invoke()
  init {
    getHomePage()
    updateToken()
  }

  private fun updateToken() {
    homeUseCase.updateToken()
      .launchIn(viewModelScope)
  }

  private fun getHomePage() {
    homeUseCase.homeService()
      .onEach { result ->
        _homeResponse.value = result
      }
      .launchIn(viewModelScope)

  }


//    set(value) {
//      adapter.differ.submitList(value.Product)
//      notifyPropertyChanged(BR.adapter)
//      CategoriesAdapter.differ.submitList(value.aClasses)
//      notifyPropertyChanged(BR.groupsAdapter)
//      homeSliderAdapter.update(value.sliders)
//      field = value
//    }


  private fun setupSlider() {
    homeData.get()?.banners?.let { homeSliderAdapter.update(it) }
    notifyPropertyChanged(BR.homeSliderAdapter)
  }

  private  val TAG = "HomeViewModel"
  fun setupCompanies() {
    Log.d(TAG, "setupCompanies: "+homeData.get()!!.companies.size)
    homeData.get()?.companies?.let { adapter.differ.submitList(it) }
//    adapter.differ.submitList(homeData.get().companies)
    notifyPropertyChanged(BR.adapter)
  }

  private fun setupCategories() {
    Log.d(TAG, "setupCategories: HERERERERE")
    Log.d(TAG, "setupCategories: "+homeData.get()!!.categories.size)
    homeData.get()?.categories.let { categoriesImageAdapter.differ.submitList(it) }
    Log.d(TAG, "setupCategories: ${categoriesImageAdapter.differ.currentList.size}")
    notifyPropertyChanged(BR.categoriesImageAdapter)
//    notifyPropertyChanged(BR.CategoriesAdapter)
  }

  fun setData(value: HomeResponse) {
    Log.d(TAG, "setData: starting here")
    this.homeData.set(value)
    setupSlider()
    setupCompanies()
    setupCategories()
  }

  fun showAllCompanies(v: View){
    v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToCompaniesFragment())
  }
}
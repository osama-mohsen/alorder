package app.grand.tafwak.presentation.home.viewModels

import android.util.Log
import android.view.View
import android.widget.Button
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.cart.adapters.OrderCartBodyAdapter
import app.grand.tafwak.presentation.section_details.SectionDetailsFragment
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ItemProductViewModel(
  val product: Product,
  val productLocalRepository: ProductLocalRepository,
  val showAddButton:Boolean = true
) : BaseViewModel() {
  @Bindable
  val adapter: OrderCartBodyAdapter = OrderCartBodyAdapter()
  val btnAdd = ObservableBoolean(false)
  val btnCounter = ObservableBoolean(false)
  val counter = ObservableInt(0)

  private val TAG = "ItemProductViewModel"

  init {
//    setupProduct()
    viewModelScope.launch {
      val productExist = productLocalRepository.isExist(product.id)
      withContext(Dispatchers.IO) {
        productExist.collect {
          when (it) {
            1 -> {
              withContext(Dispatchers.IO) {
                productLocalRepository.getProduct(product.id).collect { productExist ->
                  if(productExist != null && productExist.quantity != null) {
                    btnAdd.set(false)
                    btnCounter.set(true)
                    counter.set(productExist.quantity)
                  }
                }
                cancel()
              }
            }
            else -> {
              //not-exist
              btnAdd.set(true)
              btnCounter.set(false)
            }
          }
        }
        cancel()
      }
    }
  }

//  private fun setupProduct() {
//    val list = ArrayList<Product>()
//    adapter.differ.submitList(list)
//    notifyPropertyChanged(BR.adapter)
//  }

  fun addToCart(v: View){
    Log.d(TAG, "addToCart: ${counter.get()}")
    btnAdd.set(false)
    btnCounter.set(true)
    counter.set(counter.get())
    add(v)
  }

  fun add(v: View) {
    counter.set(counter.get() + 1)
    Log.d(TAG, "add: ")
    viewModelScope.launch {
      if(product.quantity == 0) product.quantity = 1
      Log.d(TAG, "add: ${product.quantity}")
      productLocalRepository.insert(product)
    }
  }

  fun minus(v: View) {
    if (counter.get() > 1) {
      counter.set(counter.get() - 1)
    }else{
      resetButtonAdd()
//      v.findFragment<SectionDetailsFragment>().viewModel.resetButtonAdd(product.id)
      if(v.findNavController().currentDestination?.id == R.id.sectionDetailsFragment)
        v.findFragment<SectionDetailsFragment>().onResume()

    }
    viewModelScope.launch {
      productLocalRepository.delete(product.id)
    }
  }

  fun resetButtonAdd(){
    counter.set(0)
    btnAdd.set(true)
    btnCounter.set(false)
  }
}
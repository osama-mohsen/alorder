package app.grand.tafwak.presentation.cart.viewModel

import android.app.DatePickerDialog
import android.os.CancellationSignal
import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import androidx.sqlite.db.SupportSQLiteCompat.Api16Impl.cancel
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.CompanyCart
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.cart.CartFragment
import app.grand.tafwak.presentation.cart.adapters.CartProductAdapter
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import java.text.SimpleDateFormat
import java.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ItemCartCompanyViewModel(
  val companyCart: CompanyCart,
  val position: Int,
  val productLocalRepository: ProductLocalRepository,
  val userLocalUseCase: UserLocalUseCase,
) :
  BaseViewModel() {

  val showDetails = ObservableBoolean(position == 0)
  val showDebit = ObservableBoolean(false)

  @Bindable
  var adapter: CartProductAdapter = CartProductAdapter(productLocalRepository)

  var date = ObservableField("")
  val paymentMethodShow = ObservableBoolean(userLocalUseCase.invoke().allowDeferredPayment != 0)


//  val counter = ObservableInt(1)

  private val TAG = "ItemCartCompanyViewMode"
  init {
    Log.d(TAG, "${userLocalUseCase.invoke().allowDeferredPayment}: ")
    adapter.differ.submitList(companyCart.products)
    notifyPropertyChanged(BR.adapter)
  }

  fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
    Log.d(TAG, "onSplitTypeChanged: ")
    when (id) {
      R.id.radio_cash -> {
        showDebit.set(false)
        radioGroup?.findFragment<CartFragment>()?.viewModel!!.setPaymentMethod(companyCart.id, 1)
      }
      else -> {
        Log.d(TAG, "onSplitTypeChanged: DEBIT")
        showDebit.set(true)
        radioGroup?.findFragment<CartFragment>()?.viewModel!!.setPaymentMethod(companyCart.id, 2)
      }
    }
  }


  fun showDetails() {
    this.showDetails.set(!this.showDetails.get())
  }


  fun selectDate(v: View) {
    val myCalendar: Calendar = Calendar.getInstance();

    val date: DatePickerDialog.OnDateSetListener =
      DatePickerDialog.OnDateSetListener { p0, p1, p2, p3 ->
        myCalendar.set(Calendar.YEAR, p1)
        myCalendar.set(Calendar.MONTH, p2)
        myCalendar.set(Calendar.DAY_OF_MONTH, p3)
        val myFormat = "yy-MM-dd"
        val dateFormat = SimpleDateFormat(myFormat, Locale.US)
        this.date.set(dateFormat.format(myCalendar.time))
        v.findFragment<CartFragment>().viewModel.setDate(companyCart.id, date.get()!!)
      }
    val datePicker = DatePickerDialog(
      v.context, date,
      myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH], myCalendar[Calendar.DAY_OF_MONTH]
    )
    datePicker.datePicker.minDate = System.currentTimeMillis() - 1000;

    datePicker.show()
  }

  fun delete(v: View){

    viewModelScope.launch {
      withContext(Dispatchers.IO) {
        Log.d(TAG, "delete: ${companyCart.id}")
        productLocalRepository.removeCompany(companyCart.id)
        cancel(CancellationSignal())
      }
      cancel(CancellationSignal())
//      v.findFragment<CartFragment>().viewModel.adapter.remove(position)
    }
    v.findFragment<CartFragment>().viewModel.adapter.delete(position)
    v.findFragment<CartFragment>().viewModel.notifyChanges()
  }
}
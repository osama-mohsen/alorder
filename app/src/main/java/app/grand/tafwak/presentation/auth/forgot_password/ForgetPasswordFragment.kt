package app.grand.tafwak.presentation.auth.forgot_password

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import com.structure.base_mvvm.databinding.FragmentForgotPasswordBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ForgetPasswordFragment : BaseFragment<FragmentForgotPasswordBinding>() {
  private val viewModel: ForgetPasswordViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_forgot_password

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.forgetPasswordResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            openConfirmCode(it.value.userId)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
          Resource.Default -> {}
        }
      }
    }
  }

  private fun openConfirmCode(userId : Int) {
    navigateSafe(
      ForgetPasswordFragmentDirections.actionForgotPasswordFragmentToFragmentConfirmCode(
        viewModel.request.phone,
        Constants.FORGET,
        userId
      )
    )
  }
}
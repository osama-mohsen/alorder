package app.grand.tafwak.presentation.deferred

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.domain.utils.Resource
import codes.grand.pretty_pop_up.PrettyPopUpHelper
import app.grand.tafwak.presentation.auth.AuthActivity
import app.grand.tafwak.presentation.auth.log_in.LogInFragmentDirections
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.home.HomeActivity
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentAccountBinding
import com.structure.base_mvvm.databinding.FragmentDeferredBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class DeferredFragment : BaseFragment<FragmentDeferredBinding>() {

  val args : DeferredFragmentArgs by navArgs()

  val viewModel: DeferredViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_deferred

  private val TAG = "DeferredFragment"
  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    Log.d(TAG, "setBindingVariables: ${args.deferred.remainigAmounts.size}")
    viewModel.setData(args.deferred.remainigAmounts)
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this,{
      if(it == Constants.ORDERS){
        viewModel.listDeferred.forEach {
          viewModel.listDifferPopUp.add(getString(R.string.order_number,it.orderId))
        }
      }
    })
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            showMessage(it.value.message)
            viewModel.removeOrder()
            if(viewModel.listDeferred.size == 0)
              findNavController().navigateUp()
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
  }

  override fun onResume() {
    super.onResume()
  }
}
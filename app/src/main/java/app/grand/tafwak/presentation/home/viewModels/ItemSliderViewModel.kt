package app.grand.tafwak.presentation.home.viewModels

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.navigation.findNavController
import app.grand.tafwak.domain.home.models.Banner
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.companies.CompaniesFragmentDirections
import app.grand.tafwak.presentation.home.HomeFragmentDirections

class ItemSliderViewModel  constructor(val slider: Banner) : BaseViewModel(){
  fun submit(v: View){
    when(slider.type){
      "external" -> {
        var urlIntent = slider.link
        if (!slider.link.startsWith("http://") && !slider.link.startsWith("https://")) {
          urlIntent = "http://$slider.link"
        }
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(urlIntent))
        v.context.startActivity(browserIntent)
      }
      "products" -> {}
      "categories" -> {
        v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToSectionDetailsFragment(slider.link.toInt(),""))
      }
      "companies" -> {
        val action = HomeFragmentDirections.actionHomeFragmentToProductListFragment()
        action.companyId = slider.link.toInt()
        action.toolbarName = ""
        v.findNavController().navigate(action)
      }
    }
  }
}
package app.grand.tafwak.presentation.home.viewModels

import android.net.Uri
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import app.grand.tafwak.domain.home.models.Company
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.companies.CompaniesFragmentDirections
import app.grand.tafwak.presentation.home.HomeFragment
import app.grand.tafwak.presentation.home.HomeFragmentDirections
import app.grand.tafwak.presentation.my_orders.MyOrdersFragmentDirections
import app.grand.tafwak.presentation.section_details.SectionDetailsFragment
import com.structure.base_mvvm.R

class ItemCompaniesViewModel constructor(
  val companies: Company,
  val position: Int = -1,
  val selected: Boolean = false
) : BaseViewModel() {

  private val TAG = "ItemCompaniesViewModel"

  init {
    Log.d(TAG, ": " + companies.name)
  }

  fun productList(v: View) {

    val action = CompaniesFragmentDirections.actionCompaniesFragmentToProductListFragment()
    action.companyId = companies.id
    action.toolbarName = companies.name
    v.findNavController().navigate(action)
  }

  fun select(v: View) {
    val fragment = v.findNavController().currentDestination
//    Log.d(TAG, "select: ${fragment.id}")
    when (fragment?.id) {
      R.id.sectionDetailsFragment -> v.findFragment<SectionDetailsFragment>().viewModel.setSelectCompany(
        companies,
        position
      )
      R.id.home_fragment -> {
        val uri = Uri.Builder()
          .scheme("productCompany")
          .authority("app.alorder.company.products")
          .appendPath(companies.name)
          .appendPath(companies.id.toString())
          .build()
        val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
        v.findNavController().navigate(request)

      }
    }
  }
}
package app.grand.tafwak.presentation.auth.sign_up

import android.view.View
import android.webkit.URLUtil
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.auth.entity.request.RegisterRequest
import app.grand.tafwak.domain.auth.use_case.RegisterUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.domain.utils.isValidEmail
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(private val registerUseCase: RegisterUseCase) :
  BaseViewModel() {
  val request = RegisterRequest()
  private val _registerResponse = MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val registerResponse = _registerResponse
  var validationException = SingleLiveEvent<Int>()

  fun register(v: View) {
    if (request.name.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_name));
      return
    }
//    if (request.email.trim().isEmpty()) {
//      v.context.showError(v.context.getString(R.string.please_enter_your_email));
//      return
//    }
    if (request.email.trim().isNotEmpty() && !request.email.isValidEmail()) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_email));
      return
    }
    if (request.phone.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_phone));
      return
    }
    if (request.phone.trim().length != 11) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_phone));
      return
    }
    if (request.password.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_password));
      return
    }
    if (request.password.trim().length < 6) {
      v.context.showError(v.context.getString(R.string.password_number_at_least_six_character));
      return
    }
    if (request.password2.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_confirm_password));
      return
    }
    if (request.password.trim()!= request.password2.trim()) {
      v.context.showError(v.context.getString(R.string.both_passwords_must_be_same));
      return
    }
    registerUseCase(request)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        _registerResponse.value = result
      }
      .launchIn(viewModelScope)
  }

}
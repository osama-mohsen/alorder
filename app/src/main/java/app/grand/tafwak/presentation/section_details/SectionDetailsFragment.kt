package app.grand.tafwak.presentation.section_details

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.section_details.viewModels.SectionDetailsViewModel
import com.structure.base_mvvm.databinding.FragmentSectionDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import android.opengl.ETC1.getHeight
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


@AndroidEntryPoint
class SectionDetailsFragment : BaseFragment<FragmentSectionDetailsBinding>() {

  val viewModel: SectionDetailsViewModel by viewModels()
  val args: SectionDetailsFragmentArgs by navArgs()

  override
  fun getLayoutId() = R.layout.fragment_section_details

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.id = args.categoryId
    viewModel.getProducts()
    viewModel.getPopularProducts()
    binding.rvPopularProducts.layoutManager = object : LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false) {
      override fun checkLayoutParams(lp: RecyclerView.LayoutParams?): Boolean {
        lp?.let {
          lp.width = width/2
        }
        return true
      }
    }
    binding.rvPopularProducts.adapter = viewModel.productsPopularAdapter
//    viewModel.productsPopularAdapter.showAddButton = false

  }

  override
  fun setupObservers() {

    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setData(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

    lifecycleScope.launchWhenResumed {
      viewModel.responsePopularProducts.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            Log.d(TAG, "setupObservers: WORKED DONE")
            hideLoading()
            viewModel.setPopularProducts(it.value.products)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.responseProducts.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            Log.d(TAG, "setupObservers: WORKED there")
            hideLoading()
            viewModel.setData(it.value)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
    binding.orderTotalContainer.setOnClickListener {
      try{
        navigateSafe(SectionDetailsFragmentDirections.actionSectionDetailsFragmentToCartFragment())
      }catch (e: Exception){
        e.printStackTrace()
      }
    }
//    viewModel.productsAdapter.clickEvent.observe(this,{
//      Log.d(TAG, "setupObservers: HERE")
//      viewModel.addProduct(it)
//    })



    Log.d(TAG, "setupObservers: WorkedHere")
//    binding.nestedScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
//      if (v.getChildAt(v.childCount - 1) != null) {
//        if (scrollY >= v.getChildAt(v.childCount - 1)
//            .measuredHeight - v.measuredHeight &&
//          scrollY > oldScrollY
//        ) {
//          Log.d(TAG, "setupObservers: StartingCall")
//          viewModel.callService()
//          //code to fetch more data for endless scrolling
//        }
//      }
//    } as NestedScrollView.OnScrollChangeListener)

    Log.d(TAG, "setupObservers: worked in product")
    binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        Log.d(TAG, "onScrollStateChanged: START")
        if (!recyclerView.canScrollVertically(1)) {
          Log.d(TAG, "onScrollStateChanged: CALLLLLLL")
          viewModel.getProducts()
        }
      }
    })

  }
  private  val TAG = "SectionDetailsFragment"

  override fun onResume() {
    super.onResume()
    viewModel.notifyProducts()
  }
}
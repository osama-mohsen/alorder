package app.grand.tafwak.presentation.account

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showInfo
import app.grand.tafwak.domain.account.use_case.AccountUseCases
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.use_case.RegisterUseCase
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.account.adapter.AccountAdapter
import app.grand.tafwak.presentation.base.BaseViewModel
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.lang.Exception
import java.net.URLEncoder
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
  private val registerUseCase: RegisterUseCase,
  val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {
  var phone:String = ""
  @Bindable
  val accountAdapter = AccountAdapter()
  val response = MutableStateFlow<Resource<DeferredPaymentsResponse>>(Resource.Default)
  var name = userLocalUseCase.invoke().name


  @Bindable
  var imageUrl = ""

  init {
    loadLocalImage()
  }

  fun loadLocalImage(){
    userLocalUseCase.invoke().profileImg?.let {
      imageUrl = it
      notifyPropertyChanged(BR.imageUrl)
    }
  }

  fun getDeferredPayments() {
    registerUseCase.getDeferredPayments().onEach {
      response.value = it
    }.launchIn(viewModelScope)
  }

  fun logOut() {
    userLocalUseCase.clearUser()
  }

  fun clearStorage() {
    viewModelScope.launch {
//      userLocalUseCase.logOut()
    }
  }

  fun call(v: View) {
    val call = Uri.parse("tel:$phone")
    val surf = Intent(Intent.ACTION_DIAL, call)
    v.context.startActivity(surf)
  }

  fun whatsapp(v: View) {
    val url = "https://api.whatsapp.com/send?phone=${phone}"
    val i = Intent(Intent.ACTION_VIEW)

    try {
      i.setPackage("com.whatsapp")
      i.data = Uri.parse(url)
      v.context.startActivity(i)
    } catch (e: Exception) {
      try {
        i.setPackage("com.whatsapp.w4b")
        i.data = Uri.parse(url)
        v.context.startActivity(i)
      } catch (exception: Exception) {
        v.context.showInfo(v.context.getString(R.string.please_install_whatsapp_on_your_phone));
      }
    }
  }
}
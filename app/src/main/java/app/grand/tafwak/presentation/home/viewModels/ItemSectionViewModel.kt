package app.grand.tafwak.presentation.home.viewModels

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableInt
import app.grand.tafwak.domain.home.models.Category
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.cart.adapters.OrderCartBodyAdapter
import com.structure.base_mvvm.BR

class ItemSectionViewModel constructor(val category: Category,val selected : Boolean = false) : BaseViewModel() {
  @Bindable
  val adapter: OrderCartBodyAdapter = OrderCartBodyAdapter()

  val counter = ObservableInt(1)

  init {
    setupProduct()
  }

  private fun setupProduct() {
    val list = ArrayList<Product>()
    adapter.differ.submitList(list)
    notifyPropertyChanged(BR.adapter)
  }

  fun submit(v: View){
  }

  fun add(){
    counter.set(counter.get()+1)
  }

  fun minus(){
    if(counter.get() > 1)
      counter.set(counter.get()-1)
  }
}
package app.grand.tafwak.presentation.product

import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.MyApplication
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.section_details.adapters.CategoriesAdapter
import app.grand.tafwak.presentation.section_details.adapters.ProductsAdapter
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class CompanyProductListViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase,
  val productLocalRepository: ProductLocalRepository
) : BaseViewModel() {

  var search: String? = null

  @Bindable
  var page: Int = 0

  @Bindable
  var allowCallService = true
  @Bindable
  var last = false
  var id: Int? = null
  var companyId: Int? = null



  @Bindable
  val category: CategoriesAdapter = CategoriesAdapter()

  @Bindable
  @Inject
  lateinit var productsAdapter: ProductsAdapter

  val _responseProductService =
    MutableStateFlow<Resource<ProductsResponse>>(Resource.Default)
  val responseProducts = _responseProductService


  val _responseCompanyCategories =
    MutableStateFlow<Resource<CompaniesCategoriesResponse>>(Resource.Default)
  val responseCompanyCategories = _responseCompanyCategories



  val btnCartPrice = ObservableBoolean(false)
  val cartCount = ObservableField("")
  val cartTotal = ObservableField("")

  private val TAG = "SectionDetailsViewModel"

  var total = 0.0

  init {
    Log.d(TAG, "HERERERERER")
    viewModelScope.launch {
      val productsCount = productLocalRepository.getProductsCount()
      productsCount.collect {
        btnCartPrice.set(it > 0)
        cartCount.set(it.toString())
      }
    }
    viewModelScope.launch {
      val productsTotal = productLocalRepository.products()
      productsTotal.collect {
        total = 0.0
        it.forEach { productItem ->
          total += productItem.quantity * productItem.price
        }
        Log.d(TAG, ": $total")
        cartTotal.set(total.toString())
      }
    }

  }


  fun setData(response: ProductsResponse) {
    Log.d(TAG, "setData: HERE")
    last = (response.products.size < Constants.PAGINATION_LIMIT)
    Log.d(TAG, "setData: $last")
    allowCallService = true
    when (page) {
      1 -> {
        productsAdapter.insertData(response.products)
        notifyPropertyChanged(BR.productsAdapter)
      }
      else -> productsAdapter.insertNewData(response.products)
    }
    notifyPropertyChanged(BR.last)
    notifyPropertyChanged(BR.page)
    notifyPropertyChanged(BR.allowCallService)
  }

  fun getProducts() {
    Log.d(TAG, "getProducts: HERRE")
    if (!last && allowCallService) {
      Log.d(TAG, "getProducts: WORKED")
//      callingService = true
//      notifyPropertyChanged(BR.callingService)
      page++
      if (page > 1) {
        notifyPropertyChanged(BR.page)
        notifyPropertyChanged(BR.allowCallService)
        notifyPropertyChanged(BR.last)
      }
      allowCallService = false
      homeUseCase.products(id,"", companyId,null, page,page == 1)
        .onEach {
          responseProducts.value = it
        }
        .launchIn(viewModelScope)
    }
  }

  fun getCompanyProducts(){
    homeUseCase.companiesCategories(companyId!!)
      .onEach {
        responseCompanyCategories.value = it
      }
      .launchIn(viewModelScope)
  }

  fun resetData() {
    page = 0
    allowCallService = true
    last = false
  }

  fun setCategoriesCompanies(companiesCategoriesResponse: CompaniesCategoriesResponse) {
    Log.d(TAG, "setCategoriesCompanies: ${companiesCategoriesResponse.categories.size}")
    val categories = ArrayList<Category>()
    companiesCategoriesResponse.categories.forEach {
      categories.add(addCategory(it.categoryId,it.name))
    }
    if(categories.isNotEmpty()){
      categories.add(0,addCategory(-1,MyApplication.instance.getString(R.string.all)))
      category.position = 0
//      id = categories[0].id
      getProducts()
    }
    category.differ.submitList(categories)
    notifyPropertyChanged(BR.category)
  }

  fun addCategory(id: Int , name : String) : Category{
    val category = Category()
    category.id = id
    category.name = name
    return category
  }

  fun setSelectCategory(category: Category, position: Int) {
    resetData()
    this.category.setPositionSelect(position)
    when{
      position == 0 -> id = null
      else -> {
        this.id = category.id
        Log.d(TAG, "setSelectCategory: ${this.id}")
      }
    }
    getProducts()
  }
}
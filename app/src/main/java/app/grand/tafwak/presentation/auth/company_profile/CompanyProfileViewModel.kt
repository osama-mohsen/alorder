package app.grand.tafwak.presentation.auth.company_profile

import android.location.Location
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.User
import app.grand.tafwak.domain.auth.use_case.RegisterUseCase
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.helper.PopUpMenuHelper
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.extensions.showError
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CompanyProfileViewModel @Inject constructor(
  private val productLocalRepository: ProductLocalRepository,
  private val userLocalUseCase: UserLocalUseCase,
  private val registerUseCase: RegisterUseCase) :
  BaseViewModel() {
  @Bindable
  val request = AddAddress()
  lateinit var fusedLocationClient: FusedLocationProviderClient
  val progress : ObservableBoolean = ObservableBoolean(false)

  private val _registerResponse = MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val registerResponse = _registerResponse

  private val _govsResponse = MutableStateFlow<Resource<GovernoratesResponse>>(Resource.Default)
  val govsResponse = _govsResponse

  private val _citiesResponse = MutableStateFlow<Resource<CitiesResponse>>(Resource.Default)
  val citiesResponse = _citiesResponse

  var validationException = SingleLiveEvent<Int>()
  var user: User = userLocalUseCase.invoke()
  val listGovs = arrayListOf<GovernoratesResponse.Governorate>()
  val listGovsPopUp = arrayListOf<String>()
  val listCitiesPopUp = arrayListOf<String>()
  val listCities = arrayListOf<CitiesResponse.City>()
  init {
    request.phone = user.phone
    registerUseCase.getGovernorates()
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        _govsResponse.value = result
      }
      .launchIn(viewModelScope)
  }

  fun register(v: View) {
    if (request.name.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_name));
      return
    }
    if (request.address.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_address));
      return
    }
    if (request.govName.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_government));
      return
    }
    if (request.cityName.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_city));
      return
    }
    if (request.map_lat == 0.0) {
      v.context.showError(v.context.getString(R.string.please_enter_your_location));
      return
    }
    registerUseCase.addAddress(request)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        _registerResponse.value = result
      }
      .launchIn(viewModelScope)
  }
  var popUpMenuHelper: PopUpMenuHelper = PopUpMenuHelper()

  fun governmentSubmit(v: View){
    popUpMenuHelper.openPopUp(v.context, v, listGovsPopUp) { position ->
      request.gov = listGovs[position].id
      request.govName = listGovs[position].governorateNameAr
      getCities()
      notifyRequest()
    }
  }

  fun citySubmit(v: View){
    if(request.gov == 0){
      v.context.showError(v.context.getString(R.string.please_enter_your_government));
      return
    }
    showPopUpCities(v)
  }
  fun getCities(){
    registerUseCase.getCities(request.gov)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        _citiesResponse.value = result
      }
      .launchIn(viewModelScope)
  }



  fun setGovs(governorates: List<GovernoratesResponse.Governorate>) {
    listGovs.clear()
    listGovsPopUp.clear()
    listGovs.addAll(governorates)
    governorates.forEach {
      listGovsPopUp.add(it.governorateNameAr)
    }
  }

  fun setCities(cities: List<CitiesResponse.City>) {
    listCities.clear()
    listCitiesPopUp.clear()
    listCities.addAll(cities)
    cities.forEach {
      listCitiesPopUp.add(it.cityNameAr)
    }
  }
  fun showPopUpCities(v: View){
    popUpMenuHelper.openPopUp(v.context, v, listCitiesPopUp) { position ->
      request.city = listCities[position].id
      request.cityName = listCities[position].cityNameAr
      notifyPropertyChanged(BR.request)
    }
  }

  fun notifyRequest(){
    notifyPropertyChanged(BR.request)
  }
  fun enableLocation(v: View){
    v.findFragment<CompanyProfileFragment>().navigateSafe(CompanyProfileFragmentDirections.actionCompanyProfileFragmentToDetectLocationDialog())
  }

  fun clearCart() {
    viewModelScope.launch {
      productLocalRepository.clear()
    }
  }

}
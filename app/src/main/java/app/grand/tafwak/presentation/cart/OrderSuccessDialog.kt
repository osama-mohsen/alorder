package app.grand.tafwak.presentation.cart

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.cart.viewModel.OrderSuccessViewModel
import app.grand.tafwak.presentation.home.HomeActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.OrderSuccessDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderSuccessDialog : DialogFragment() {
  lateinit var binding: OrderSuccessDialogBinding
  private val viewModel: OrderSuccessViewModel by viewModels()
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    binding = DataBindingUtil.inflate(inflater, R.layout.order_success_dialog, container, false)
    binding.viewModel = viewModel
    viewModel.clickEvent.observe(this,{
      when(it){
        Constants.DISMISS -> dismiss()
        else -> {
          (requireActivity() as HomeActivity).binding.bottomNavigationView.selectedItemId = R.id.myOrdersFragment;
        }
      }
    })
    return binding.root
  }
  override fun getTheme(): Int {
    return R.style.CustomDialogTheme;
  }
}
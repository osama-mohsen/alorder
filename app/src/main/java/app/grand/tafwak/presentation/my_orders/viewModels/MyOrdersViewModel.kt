package app.grand.tafwak.presentation.my_orders.viewModels

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.domain.home.models.Companies
import app.grand.tafwak.domain.home.models.HomeResponse
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.order.OrderListResponse
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.my_orders.adapters.MyOrdersAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyOrdersViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase
) : BaseViewModel() {

  @Bindable
  val adapter: MyOrdersAdapter = MyOrdersAdapter()


  private val _homeResponse =
    MutableStateFlow<Resource<OrderListResponse>>(Resource.Default)
  val homeResponse = _homeResponse

  init {
  }
  fun callService(){
    homeUseCase.orders()
      .onEach {
        _homeResponse.value = it
      }.launchIn(viewModelScope)
  }

  fun setData(orders: OrderListResponse){
    adapter.differ.submitList(orders.orders)
    notifyPropertyChanged(BR.adapter)
  }

}
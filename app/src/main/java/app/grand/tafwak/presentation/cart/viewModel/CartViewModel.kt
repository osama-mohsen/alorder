package app.grand.tafwak.presentation.cart.viewModel

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.core.extenstions.showInfo
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.cart.Cart
import app.grand.tafwak.domain.cart.ConfirmPasswordRequest
import app.grand.tafwak.domain.home.models.CompanyCart
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.base.utils.showMessage
import app.grand.tafwak.presentation.cart.adapters.CartCompanyAdapter
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
  val productLocalRepository: ProductLocalRepository,
  private val homeUseCase: HomeUseCase,
  val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {

  var adapter: CartCompanyAdapter = CartCompanyAdapter(productLocalRepository,userLocalUseCase)

  val requestPassword = ConfirmPasswordRequest()

  private val _response =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)

  val passswordConfirm =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)

  val response = _response
  private val TAG = "CartViewModel"

  val cartTotal = ObservableField("")
  var total = 0.0
  val cartRequest = ArrayList<Cart>()

  init {
    val list = ArrayList<CompanyCart>()
    val hashMap = HashMap<Int, ArrayList<Product>>()
    val companyTotal = HashMap<Int, Double>()

    viewModelScope.launch {
      withContext(Dispatchers.IO) {
        Log.d(TAG, "setupCart: HERE")
        productLocalRepository.products().collect {
          Log.d(TAG, "size: ${it.size}")
//          Log.d(TAG, ": ${it.size}")
          companyTotal.clear()
          hashMap.clear()
          list.clear()
          it.forEach {
            Log.d(TAG, ": ${it.name}")
            Log.d(TAG, ": ${it.companyId}")
            if (!hashMap.containsKey(it.companyId)) {
              hashMap[it.companyId] = arrayListOf()
              companyTotal[it.companyId] = 0.0
            }
            val products = hashMap[it.companyId]
            products?.add(it)
            products?.let { it1 ->
              hashMap[it.companyId] = it1
            }
            companyTotal[it.companyId] = (companyTotal[it.companyId]!! + (it.price * it.quantity))
          }
          cartRequest.clear()

          Log.d(TAG, ": ${hashMap.size}")
          hashMap.forEach {
            if (it.value.size > 0) {
              Log.d(TAG, ": ")
              list.add(
                CompanyCart(
                  it.key,
                  it.value[0].companyName,
                  it.value[0].companyImage,
                  companyTotal[it.key]!!,
                  it.value
                )
              )
              /*
              var products: ArrayList<Product> = ArrayList()
    var total: String = ""
    var paid_amount: String = ""
    var payment_method: String = ""
    var delivery_date: String = ""
    var company_id: Int = -1
               */
              cartRequest.add(
                Cart(products = it.value, companyTotal[it.key]!!, company_id = it.key)
              )
            }
          }
          adapter.submitList(list)
          notifyChanges()
          cancel()
        }
        cancel()
      }
    }


    viewModelScope.launch {
      val productsTotal = productLocalRepository.products()
      Log.d(TAG, "TOTAL HERE: ")
      productsTotal.collect {
        Log.d(TAG, "TOTAL: ${it.size}")
        total = 0.0
        it.forEach { productItem ->
          total += productItem.quantity * productItem.price
        }
        Log.d(TAG, ": $total")
        cartTotal.set(total.toString())
        if (total == 0.0)
          clickEvent.value = Constants.BACK
      }
    }

  }


  fun requestPassword(v: View){
    if(requestPassword.password.trim().isEmpty())
      showMessage(v.context,v.context.getString(R.string.please_enter_your_password_to_confirm_your_order))
    else{
      homeUseCase.submitPassword(requestPassword)
        .onEach {
          passswordConfirm.value = it
        }
        .launchIn(viewModelScope)
    }
  }

  fun submitOrder(){
    homeUseCase.submitCart(cartRequest)
      .onEach {
        _response.value = it
      }
      .launchIn(viewModelScope)
  }

  fun notifyChanges(){
    notifyPropertyChanged(BR.adapter)
  }

  fun next(v: View) {
    Log.d(TAG, "next: ${userLocalUseCase.invoke().status}")
    if(userLocalUseCase.invoke().status == 0){
      v.context.showInfo(v.context.getString(R.string.you_dont_have_permission_for_submitting_your_order_please_contact_support));
      return
    }
    cartRequest.forEachIndexed { index, it ->
      if (it.delivery_date == "") {
        v.context.showError(
          v.resources.getString(R.string.please_enter_your_date) + " " + v.resources.getString(
            R.string.order_title
          ) + " " + (index + 1)
        )
        return
      }
      if (it.payment_method == 2) { // pay debit
        if (it.paid_amount == "") {
          adapter.differ.currentList.forEach { companyCart ->
            if (companyCart.id == it.company_id)
              it.paid_amount = companyCart.willPay
          }
          if (it.paid_amount == "") {
            v.context.showError(
              v.resources.getString(R.string.please_enter_your_amount) + " " + v.resources.getString(
                R.string.order_title
              ) + " " + (index + 1)
            )
            return
          }else if(it.paid_amount.isNotEmpty()){
            try{
              if(it.paid_amount.toDouble() > it.total){
                v.context.showError(
                  v.resources.getString(R.string.max_limit_for_paying) + " "+it.total+" "+ v.resources.getString(
                    R.string.order_title
                  ) + " " + (index + 1)
                )
                return
              }
            }catch (e: Exception){
              e.printStackTrace()
            }
          }
        }
      }
    }
    Log.d(TAG, "next: HERE")
    clickEvent.value = Constants.ACTION
//    submitOrder()
  }

  fun setPaymentMethod(companyId: Int, paymentMethod: Int) {
    cartRequest.forEach {
      if (it.company_id == companyId)
        it.payment_method = paymentMethod
    }
  }

  fun setDate(companyId: Int, date: String) {
    cartRequest.forEach {
      if (it.company_id == companyId)
        it.delivery_date = date
    }
  }

  fun clearCart() {
    viewModelScope.launch {
      withContext(Dispatchers.IO){
        productLocalRepository.clear()
      }
      cancel()
    }
  }

}
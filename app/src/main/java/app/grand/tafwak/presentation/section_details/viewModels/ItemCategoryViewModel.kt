package app.grand.tafwak.presentation.section_details.viewModels

import android.util.Log
import android.view.View
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import app.grand.tafwak.domain.home.models.Category
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.product.CompanyProductListFragment
import app.grand.tafwak.presentation.section_details.SectionDetailsFragment
import com.structure.base_mvvm.R

class ItemCategoryViewModel constructor(val category: Category,val position: Int, val selected : Boolean = false) : BaseViewModel() {

  private val TAG = "ItemCategoryViewModel"
  fun select(v: View){
    val fragment = v.findNavController().currentDestination
    when(fragment?.id){
      R.id.sectionDetailsFragment -> {
          v.findFragment<SectionDetailsFragment>().viewModel.setSelectCategory(category,position)
      }
      R.id.companyProductListFragment -> {
        Log.d(TAG, "select: ${category.id}")
        v.findFragment<CompanyProductListFragment>().viewModel.setSelectCategory(category,position)
      }
    }
  }
}
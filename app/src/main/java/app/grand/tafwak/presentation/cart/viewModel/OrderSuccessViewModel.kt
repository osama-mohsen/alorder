package app.grand.tafwak.presentation.cart.viewModel

import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.navigation.findNavController
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.cart.OrderSuccessDialogDirections
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OrderSuccessViewModel @Inject constructor(
  val userLocalUseCase: UserLocalUseCase,
) : BaseViewModel() {

  var name = userLocalUseCase.invoke().name
  private  val TAG = "OrderSuccessViewModel"
  fun orderDetails(v: View){
    clickEvent.value = Constants.ORDERS
  }

  fun cancel(v: View){
    clickEvent.value = Constants.DISMISS
//    v.findNavController().popBackStack()
  }
}
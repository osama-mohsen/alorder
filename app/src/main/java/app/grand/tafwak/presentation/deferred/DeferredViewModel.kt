package app.grand.tafwak.presentation.deferred

import android.util.Log
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.MyApplication
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.AccountUseCases
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.use_case.RegisterUseCase
import app.grand.tafwak.domain.profile.entity.DeferredModel
import app.grand.tafwak.domain.profile.entity.DeferredPaymentsResponse
import app.grand.tafwak.domain.profile.entity.PayDeferredRequest
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.helper.PopUpMenuHelper
import app.grand.tafwak.presentation.account.adapter.AccountAdapter
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DeferredViewModel @Inject constructor(
  private val registerUseCase: RegisterUseCase,
  val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {
  val listDeferredFinished = ArrayList<Int>()
  val listDeferred = ArrayList<DeferredModel>()
  val response = MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val deferred = ObservableField<DeferredModel>()
  val request = PayDeferredRequest()
  val order = ObservableField("")
  val totalAmount = ObservableField("")
  val paid = ObservableField("")
  val reset = ObservableField("")

  val listDifferPopUp = arrayListOf<String>()
  var popUpMenuHelper: PopUpMenuHelper = PopUpMenuHelper()
  var lastPosition = -1

  init {
  }


  fun setData(list: ArrayList<DeferredModel>){
    Log.d(TAG, "setData: ${list.size}")
    listDeferred.clear()
    listDifferPopUp.clear()
    listDeferred.addAll(list)
    clickEvent.value = Constants.ORDERS
    Log.d(TAG, "setDataPop: ${listDifferPopUp.size}")
  }
  private val TAG = "DeferredViewModel"
  fun selectOrder(v: View){
    Log.d(TAG, "selectOrder: ")
    Log.d(TAG, "selectOrder: ${listDifferPopUp.size}")
    popUpMenuHelper.openPopUp(v.context, v, listDifferPopUp) { position ->
      Log.d(TAG, "selectOrder: SELECT")
      order.set(listDeferred[position].orderId.toString())
      totalAmount.set(listDeferred[position].orderTotal.toString())
      paid.set(listDeferred[position].paidAmount.toString())
      reset.set(listDeferred[position].remainingAmount.toString())
      request.order_id = listDeferred[position].orderId
      request.amount = listDeferred[position].remainingAmount.toDouble()
      lastPosition = position
    }
  }

  fun submitPay(v: View){
    if(request.order_id != 0) {
      registerUseCase.payDeferred(request).onEach {
        response.value = it
      }.launchIn(viewModelScope)
    }else{
      v.context.showError(v.context.getString(R.string.please_select_your_order))
    }

  }

  fun removeOrder() {
    Log.d(TAG, "removeOrder: REMOVING ${lastPosition} , ${listDeferred.size}")
    if(lastPosition != -1 && lastPosition < listDeferred.size){
      listDeferred.removeAt(lastPosition)
      val list = ArrayList(listDeferred)
      Log.d(TAG, "removeOrder: ${listDeferred.size}")
      setData(list)
      resetData()
    }
  }

  private fun resetData() {
    order.set("")
    totalAmount.set("")
    paid.set("")
    reset.set("")
  }
}
package app.grand.tafwak.presentation.auth.confirmCode

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.handleApiError
import app.grand.tafwak.presentation.base.extensions.hideKeyboard
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.utils.Constants
import com.structure.base_mvvm.databinding.FragmentConfirmCodeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ConfirmCodeFragment : BaseFragment<FragmentConfirmCodeBinding>() {

  private val viewModel: ConfirmViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_confirm_code

  override
  fun setBindingVariables() {
    binding.viewmodel = viewModel
  }

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.verifyResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            openChangePassword(viewModel.request.user_id)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
          Resource.Default -> {
          }
        }
      }
    }
  }

  private fun openChangePassword(userId: Int) {
    val action = ConfirmCodeFragmentDirections.actionFragmentConfirmCodeToChangePasswordFragment(Constants.AUTH_STRING)
    action.userId = userId
    navigateSafe(action)
  }


}
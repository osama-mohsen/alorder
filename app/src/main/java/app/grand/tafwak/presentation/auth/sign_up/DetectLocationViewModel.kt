package app.grand.tafwak.presentation.auth.sign_up

import android.location.Location
import android.util.Log
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.CitiesResponse
import app.grand.tafwak.domain.auth.entity.model.GovernoratesResponse
import app.grand.tafwak.domain.auth.entity.model.User
import app.grand.tafwak.domain.auth.use_case.RegisterUseCase
import app.grand.tafwak.domain.profile.entity.AddAddress
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.helper.PopUpMenuHelper
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.extensions.showError
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.base.utils.Constants.LOCATION
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class DetectLocationViewModel @Inject constructor(
  private val userLocalUseCase: UserLocalUseCase,
) : BaseViewModel() {
  val permissions = ArrayList<Boolean>()
  lateinit var fusedLocationClient: FusedLocationProviderClient
  val progress: ObservableBoolean = ObservableBoolean(false)


  lateinit var latLng: LatLng

  fun detectLocation(v: View) {
    Log.d(TAG, "detectLocation: STart")
    val cancellationToken = object : CancellationToken() {
      override fun onCanceledRequested(listener: OnTokenCanceledListener): CancellationToken =
        this

      override fun isCancellationRequested(): Boolean = false
    }
    progress.set(true)
    fusedLocationClient.getCurrentLocation(
      LocationRequest.PRIORITY_HIGH_ACCURACY,
      cancellationToken
    ).addOnSuccessListener { location: Location? ->
      progress.set(false)
      if (location == null) {
        v.context.showError(v.context.getString(R.string.failed_to_fetch_your_location))
      } else {
        latLng = LatLng(location.latitude, location.longitude)
        submitEvent.value = LOCATION
      }
    }
  }

  private  val TAG = "DetectLocationViewModel"
  fun checkDetectLocation(v: View) {
    var counter = 0
    Log.d(TAG, "checkDetectLocation: ${permissions.size}")
    permissions.forEach {
      if(it) counter++
    }
    Log.d(TAG, "checkDetectLocation: ${counter}")
    if(counter == permissions.size)
      detectLocation(v)
  }

}
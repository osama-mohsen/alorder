package app.grand.tafwak.presentation.auth.sign_up

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.motion.widget.Debug
import androidx.constraintlayout.motion.widget.Debug.getLocation
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extenstions.getLocation
import app.grand.tafwak.presentation.auth.company_profile.CompanyProfileViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DetectLocationDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetectLocationDialog : BottomSheetDialogFragment() {
  private val viewModel: DetectLocationViewModel by viewModels()
  lateinit var binding: DetectLocationDialogBinding

  private var requestMultiplePermissions =
    registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
      permissions.entries.forEach {
        viewModel.permissions.add(it.value)
      }
      viewModel.checkDetectLocation(requireView())
    }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    binding =
      DataBindingUtil.inflate(inflater, R.layout.detect_location_dialog, container, false)
    viewModel.fusedLocationClient =
      LocationServices.getFusedLocationProviderClient(requireContext())

    binding.viewModel = viewModel

    viewModel.submitEvent.observe(this, {
      if (it == Constants.LOCATION) {
        val bundle = Bundle()
        bundle.putDouble(Constants.LAT, viewModel.latLng.latitude)
        bundle.putDouble(Constants.LNG, viewModel.latLng.longitude)
        val address =
          requireContext().getLocation(viewModel.latLng.latitude, viewModel.latLng.longitude)
        bundle.putString(
          Constants.ADDRESS,
          address
        )

        Log.d(TAG, "onCreateView: ${viewModel.latLng.latitude}")
        Log.d(TAG, "onCreateView: ${viewModel.latLng.longitude}")
        Log.d(TAG, "onCreateView: $address")

        val n = findNavController()
        n.navigateUp()
        n.currentBackStackEntry?.savedStateHandle?.set(
          Constants.BUNDLE,
          bundle
        )
      } else if (it == Constants.LOCATION_PERMISSION) {
        checkPermission()
      }
    })
//    binding.confirm.setOnClickListener { dismiss() }
    return binding.root
  }

  private val TAG = "DetectLocationDialog"
  fun checkPermission() {

// Usage:
    requestMultiplePermissions.launch(
      arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
      )
    )

  }

}
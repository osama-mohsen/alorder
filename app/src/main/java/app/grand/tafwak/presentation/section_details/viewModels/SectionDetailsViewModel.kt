package app.grand.tafwak.presentation.section_details.viewModels

import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.*
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.home.adapters.CompaniesAdapter
import app.grand.tafwak.presentation.section_details.adapters.CategoriesAdapter
import app.grand.tafwak.presentation.section_details.adapters.ProductsAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SectionDetailsViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase,
  val productLocalRepository: ProductLocalRepository
) : BaseViewModel() {

  @Bindable
  var page: Int = 0

  @Bindable
  var last = false
  var allowCallService = true
  var id: Int = 0
  var companyId: Int? = null
  var featured: Int? = null



  @Bindable
  val adapter: CompaniesAdapter = CompaniesAdapter()

  @Bindable
  @Inject
  lateinit var productsAdapter: ProductsAdapter

  @Bindable
  @Inject
  lateinit var productsPopularAdapter: ProductsAdapter

  val popularProductsProgress = ObservableBoolean(false)

  @Bindable
  val category: CategoriesAdapter = CategoriesAdapter()

  val data = ObservableField(CategoryDetailsResponse())

  val _responseService =
    MutableStateFlow<Resource<CategoryDetailsResponse>>(Resource.Default)

  val response = _responseService

  var type = Constants.CATEGORIES
  val _responseProductService =
    MutableStateFlow<Resource<ProductsResponse>>(Resource.Default)

  val responsePopularProducts =
    MutableStateFlow<Resource<ProductsResponse>>(Resource.Default)

  val responseProducts = _responseProductService

  val btnCartPrice = ObservableBoolean(false)
  val cartCount = ObservableField("")
  val cartTotal = ObservableField("")
  var total = 0.0


  private val TAG = "SectionDetailsViewModel"

  init {
    Log.d(TAG, "HERERERERER")
    viewModelScope.launch {
      val productsCount = productLocalRepository.getProductsCount()
      productsCount.collect {
        btnCartPrice.set(it > 0)
        cartCount.set(it.toString())
      }
    }
    viewModelScope.launch {
      val productsTotal = productLocalRepository.products()
      productsTotal.collect {
        total = 0.0
        it.forEach { productItem ->
          total += productItem.quantity * productItem.price
        }
        Log.d(TAG, ": $total")
        cartTotal.set(total.toString())
      }
    }
  }

  fun notifyProducts(){
//    val list = ArrayList(productsAdapter.differ.currentList)
//    val listPopular = ArrayList(productsPopularAdapter.differ.currentList)
//    productsAdapter.differ.submitList(null)
//    productsAdapter.differ.submitList(list)
//    productsPopularAdapter.differ.submitList(null)
//    productsPopularAdapter.differ.submitList(listPopular)
    productsAdapter.notifyDataSetChanged()
    productsPopularAdapter.notifyDataSetChanged()
  }

  fun getPopularProducts(){
    Log.d(TAG, "getPopularProducts: here")
    popularProductsProgress.set(true)
    homeUseCase.products(id,null, companyId,1, page,false)
      .onEach {
        responsePopularProducts.value = it
      }
      .launchIn(viewModelScope)
  }

  fun getProducts() {
    Log.d(TAG, "getProducts: GetProducts $last && withAllowCallService $allowCallService")
    if (!last && allowCallService) {
      Log.d(TAG, "getProducts: CallingService with page $page")
//      callingService = true
//      notifyPropertyChanged(BR.callingService)
      page++
      if (page > 1) {
        notifyPropertyChanged(BR.page)
      }
      allowCallService = false
      when {
        page == 1 && type == Constants.CATEGORIES -> {
          Log.d(TAG, "getProducts: CATEGORIES")
          homeUseCase.categoryDetails(id, page)
            .onEach {
              response.value = it
            }
            .launchIn(viewModelScope)
        }
        else -> {
          Log.d(TAG, "getProducts: Else HERE")
          homeUseCase.products(id,null, companyId,null, page,page == 1)
            .onEach {
              responseProducts.value = it
            }
            .launchIn(viewModelScope)
        }
      }
    }
  }

  private fun setupCategory() {
    category.differ.submitList(data.get()?.subCategories)
//    notifyPropertyChanged(BR.category)
    Log.e("setupCategory", "setupCategory: " + category.differ.currentList.size)
  }

  fun setupCompanies() {
    adapter.differ.submitList(data.get()?.companies)
    notifyPropertyChanged(BR.adapter)
  }

  fun setupProduct() {
    productsAdapter.differ.submitList(this.data.get()?.products)
    notifyPropertyChanged(BR.productsAdapter)
  }

  fun setData(response: CategoryDetailsResponse) {
    this.data.set(response)
    Log.d(TAG, "setData: $type")
    if(type == Constants.CATEGORIES)
      setupCategory()
    setupCompanies()
    setupProduct()
    allowCallService = true
  }

  fun setPopularProducts(response: List<Product>) {
    Log.d(TAG, "setPopularProducts: ${response.size}")
    popularProductsProgress.set(false)
    productsPopularAdapter.differ.submitList(null)
    productsPopularAdapter.differ.submitList(response)
    notifyPropertyChanged(BR.productsPopularAdapter)

  }



  fun setData(response: ProductsResponse) {
    last = (response.products.size < Constants.PAGINATION_LIMIT)
    when(page){
      1 -> {
        productsAdapter.insertData(response.products)
        notifyPropertyChanged(BR.productsAdapter)
      }
      else -> productsAdapter.insertNewData(response.products)
    }
    notifyPropertyChanged(BR.last)
  }

  fun setSelectCategory(category: Category, position: Int) {
    this.category.setPositionSelect(position)
    this.id = category.id
    Log.d(TAG, "setSelectCategory: ${this.id}")
    resetData()
    type = Constants.COMPANIES
    getProducts()
    getPopularProducts()
  }

  fun setSelectCompany(company: Company, position: Int) {
    this.adapter.setPositionSelect(position)
    resetData()
    companyId = company.id
    type = Constants.COMPANIES
    getProducts()
    getPopularProducts()
  }
  
  private fun resetData(){
    page = 0
    last = false
    allowCallService = true
  }

  fun resetButtonAdd(productId: Int) {
    productsAdapter.resetButtonAdd(productId)
    productsPopularAdapter.resetButtonAdd(productId)
  }


//  fun addProduct(productNew: Product) {
//    Log.d(TAG, "addProduct: ${productNew.id}")
//    Log.d(TAG, "addProduct: HERE")
//    viewModelScope.launch{
//      productLocalRepository.insert(productNew)
//    }
//    CoroutineScope(Dispatchers.IO).launch {
//
////      Log.d(TAG, "addProduct: ${it.}")
////      if (it == null) {
////        CoroutineScope(Dispatchers.IO).launch {
////          productLocalRepository.insert(product)
////        }
////      } else {
////        CoroutineScope(Dispatchers.IO).launch {
////          Log.d(TAG, "addProduct: WORKED")
////          product.quantity++
////        }
////      }
//    }
//
//  }
}
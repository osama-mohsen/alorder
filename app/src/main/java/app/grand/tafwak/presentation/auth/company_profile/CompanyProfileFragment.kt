package app.grand.tafwak.presentation.auth.company_profile

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.core.extenstions.actOnGetIfNotInitialValueOrGetLiveData
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.auth.sign_up.SignUpFragmentDirections
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.base.utils.getDeviceId
import app.grand.tafwak.presentation.home.HomeActivity
import com.structure.base_mvvm.databinding.FragmentCompanyProfileBinding
import com.structure.base_mvvm.databinding.FragmentSignUpBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class CompanyProfileFragment : BaseFragment<FragmentCompanyProfileBinding>() {

  private val viewModel: CompanyProfileViewModel by viewModels()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

  }

  private  val TAG = "CompanyProfileFragment"
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    actOnGetIfNotInitialValueOrGetLiveData(
      Constants.BUNDLE,
      Bundle(),
      viewLifecycleOwner,
      { it?.containsKey(Constants.LAT) == true }
    ) {
      Log.d(TAG, "onViewCreated: THERE ${it?.getDouble(Constants.LAT)}")
      Log.d(TAG, "onViewCreated: THERE ${it?.getDouble(Constants.LNG)}")
      it?.let {
        Log.d(TAG, "onViewCreated: HERE")
        if(it.containsKey(Constants.LAT)){
          Log.d(TAG, "onViewCreated: THERE ${it.getDouble(Constants.LAT)}")
          viewModel.request.map_lat = it.getDouble(Constants.LAT)
          viewModel.request.map_long = it.getDouble(Constants.LNG)
          viewModel.request.address = it.getString(Constants.ADDRESS).toString()
          viewModel.notifyRequest()
        }
      }
    }
  }
  override
  fun getLayoutId() = R.layout.fragment_company_profile

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  override
  fun setupObservers() {

    lifecycleScope.launchWhenCreated {
      viewModel.govsResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setGovs(it.value.governorates)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.citiesResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.setCities(it.value.cities)
            viewModel.showPopUpCities(binding.inputDistrict)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.registerResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            showMessage(it.value.message)
            viewModel.clearCart()
            openActivityAndClearStack(HomeActivity::class.java)
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }
  }
}
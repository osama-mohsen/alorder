package app.grand.tafwak.presentation.home.viewModels

import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.navigation.findNavController
import app.grand.tafwak.domain.home.models.Category
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.home.HomeFragmentDirections

class ItemCategoryImageViewModel constructor(val category: Category) : BaseViewModel() {

  val counter = ObservableInt(1)

  private val TAG = "ItemCategoryImageViewMo"
  init {
    Log.d(TAG, ": "+category.name)
//    setupProduct()
  }

  fun submit(v: View){
    v.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToSectionDetailsFragment(category.id,category.name))
  }

  fun add(){
    counter.set(counter.get()+1)
  }

  fun minus(){
    if(counter.get() > 1)
      counter.set(counter.get()-1)
  }
}
package app.grand.tafwak.presentation.profile.viewModels

import android.net.Uri
import android.util.Log
import android.view.View
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.SendFirebaseTokenUseCase
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.general.use_case.GeneralUseCases
import app.grand.tafwak.domain.profile.entity.AvatarResponse
import app.grand.tafwak.domain.profile.entity.UpdateProfileRequest
import app.grand.tafwak.domain.profile.use_case.ProfileUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.domain.utils.isValidEmail
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.profile.adapter.AvatarAdapter
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.structure.base_mvvm.BR
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
  val userLocalUseCase: UserLocalUseCase,
  private val profileUseCase: ProfileUseCase,

) :
  BaseViewModel() {
  var uri: Uri? = null

  @Bindable
  val request = UpdateProfileRequest()
  private val _profileResponse =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val profileResponse = _profileResponse
  private val _avatarResponse =
    MutableStateFlow<Resource<UserResponse>>(Resource.Default)
  val avatarResponse = _avatarResponse

  @Bindable
  val avatarAdapter = AvatarAdapter()

  @Bindable
  var imageUrl = ""

  init {
    request.name = userLocalUseCase.invoke().name
    request.email = userLocalUseCase.invoke().email
    request.phone = userLocalUseCase.invoke().phone
    userLocalUseCase.invoke().profileImg?.let {
      imageUrl = it
      notifyPropertyChanged(BR.imageUrl)
    }
    notifyPropertyChanged(BR.request)

  }

  fun updateProfile(){
    val user = userLocalUseCase.invoke()
    user.name = request.name
    user.phone = request.phone
    user.email = request.email
    viewModelScope.launch {
      userLocalUseCase.invoke(user)
    }
  }


  fun updateProfile(v: View) {

    if (request.name.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_name));
      return
    }
    if (request.email.isNotEmpty() && !request.email.isValidEmail()) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_email));
      return
    }
    if (request.phone.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_phone));
      return
    }
    if (request.phone.trim().length != 11) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_phone));
      return
    }

    profileUseCase.invoke(request).catch { exception ->
      Log.e(
        "updateProfile",
        "updateProfile: ${exception.printStackTrace()}"
      )
    }.onEach { result ->
      _profileResponse.value = result
    }.launchIn(viewModelScope)
  }

  fun updateAvatarAdapter(avatarList: ArrayList<AvatarResponse>) {
    avatarAdapter.differ.submitList(avatarList)
    notifyPropertyChanged(BR.avatarAdapter)
  }

  fun updateImage() {
    if(uri != null) {
      profileUseCase.updateImage(uri!!).catch { exception ->
        Log.e(
          "updateProfile",
          "updateProfile: ${exception.printStackTrace()}"
        )
      }.onEach { result ->
        _avatarResponse.value = result
      }.launchIn(viewModelScope)
    }

  }

  private val TAG = "ProfileViewModel"
  fun updateImageAfterApi(img: String){
    Log.d(TAG, "updateImageAfterApi: ${img}")
    val user = userLocalUseCase.invoke()
    user.profileImg = img
    Log.d(TAG, "updateImageAfterApi: ${user.profileImg}")
    viewModelScope.launch {
      userLocalUseCase.invoke(user)
    }
  }

}
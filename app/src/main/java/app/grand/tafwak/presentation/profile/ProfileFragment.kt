package app.grand.tafwak.presentation.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.core.extenstions.checkSelfPermissionGranted
import app.grand.tafwak.core.extenstions.showPopup
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.utils.Constants
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.base.utils.showSuccessAlert
import com.structure.base_mvvm.databinding.FragmentProfileBinding
import app.grand.tafwak.presentation.profile.viewModels.ProfileViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.util.CollectionUtils.listOf
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.io.ByteArrayOutputStream
import java.util.*

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

  private val viewModel: ProfileViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_profile

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  fun pickImageOrRequestPermissions() {
    when {
      requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
        pickImageViaChooser()
      }
      requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
        pickImage(false)
      }
      requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
        pickImage(true)
      }
      else -> {
        permissionLocationRequest.launch(
          arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
          )
        )
      }
    }
  }

  private val permissionLocationRequest = registerForActivityResult(
    ActivityResultContracts.RequestMultiplePermissions()
  ) { permissions ->
    when {
      permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.CAMERA] == true -> {
        pickImageViaChooser()
      }
      permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
        pickImage(false)
      }
      permissions[Manifest.permission.CAMERA] == true -> {
        pickImage(true)
      }
      else -> {
        showMessage(getString(R.string.you_didn_t_accept_permission))
      }
    }
  }

  private fun pickImage(fromCamera: Boolean) {
    if (fromCamera) {
      activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
    } else {
      // From gallery
      val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
      intent.type = "image/*"
      intent.action = Intent.ACTION_GET_CONTENT
      activityResultImageGallery.launch(intent)
    }
  }

  private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
    val byteArray = stream.toByteArray()
    val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

    val path = MediaStore.Images.Media.insertImage(
      requireContext().contentResolver,
      compressedBitmap,
      Date(System.currentTimeMillis()).toString() + "photo",
      null
    )
    return Uri.parse(path)
  }


  private val activityResultImageCamera = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult
      viewModel.uri = getUriFromBitmapRetrievedByCamera(bitmap)
      loadImageProfile()
      viewModel.updateImage()
    }
  }


  private val activityResultImageGallery = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val uri = it.data?.data ?: return@registerForActivityResult
      viewModel.uri = uri
      loadImageProfile()
      viewModel.updateImage()
    }
  }


  fun loadImageProfile() {
    Glide.with(this)
      .load(viewModel.uri)
      .apply(RequestOptions().centerCrop())
      .into(binding.icProfile)
  }


  private fun pickImageViaChooser() {
    val camera = getString(R.string.camera)
    val gallery = getString(R.string.gallery)

    binding.icProfile.showPopup(listOf(camera, gallery)) {
      pickImage(it.title?.toString() == camera)
    }
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      when (it) {
        Constants.BACK -> backToPreviousScreen()
        Constants.PICK_IMAGE -> pickImageOrRequestPermissions()
      }

    }
    selectedImages.observeForever { result ->
      result.path?.let { path ->
        viewModel.request.setImage(path, Constants.IMAGE)
//        binding.userImg.setImageURI(result)
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.profileResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.updateProfile()
            backToPreviousScreen()
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(
              it,
              retryAction = { viewModel.updateProfile() })
          }

        }
      }
    }

    lifecycleScope.launchWhenResumed {

      viewModel.avatarResponse.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.updateImageAfterApi(it.value.profileImg!!)
            backToPreviousScreen()
          }
          is Resource.Failure -> {
            hideLoading()
          }

        }
      }
    }
  }
}
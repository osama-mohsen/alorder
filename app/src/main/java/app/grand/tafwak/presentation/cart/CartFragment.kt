package app.grand.tafwak.presentation.cart

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.extensions.*
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.presentation.cart.viewModel.CartViewModel
import com.structure.base_mvvm.databinding.FragmentCartBinding
import com.structure.base_mvvm.databinding.FragmentPasswordBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class CartFragment : BaseFragment<FragmentCartBinding>() {

  val viewModel: CartViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_cart

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  lateinit var builder: AlertDialog

  override fun setupObservers() {
    super.setupObservers()

    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            viewModel.clearCart()
            findNavController().navigate(CartFragmentDirections.actionCartFragmentToOrderSuccessDialog())
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

    lifecycleScope.launchWhenResumed {
      viewModel.passswordConfirm.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            showLoading()
          }
          is Resource.Success -> {
            hideLoading()
            builder.let {
              if(builder.isShowing) builder.dismiss()
              viewModel.submitOrder()
            }
          }
          is Resource.Failure -> {
            hideLoading()
            handleApiError(it)
          }
        }
      }
    }

    viewModel.clickEvent.observe(this, {
      when (it) {
        Constants.BACK ->
          findNavController().popBackStack()
        else -> {
          builder = AlertDialog.Builder(context).create()

           val binding = FragmentPasswordBinding.inflate(LayoutInflater.from(context), binding.cart, false)
          binding.viewModel = viewModel

          builder.setView(binding.root)

          builder.show()

//          if(!progressDialog.isShowing) {
//            if (progressDialog.window != null) {
//              progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            }
//            val dialogPasswordBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_password,binding.container,context, binding.)
//
//            dialogPasswordBinding.viewModel = viewModel
//            progressDialog.setContentView(dialogPasswordBinding.rootView)
//            progressDialog.setCancelable(true)
//            progressDialog.setCanceledOnTouchOutside(false)
//            progressDialog.show()
//          }
        }
      }
    })

  }


}
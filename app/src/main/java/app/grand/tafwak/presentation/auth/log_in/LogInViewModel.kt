package app.grand.tafwak.presentation.auth.log_in

import android.util.Log
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.MyApplication
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.account.use_case.UserLocalUseCase
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.LogInRequest
import app.grand.tafwak.domain.auth.use_case.LogInUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.extensions.navigateSafe
import app.grand.tafwak.presentation.base.extensions.openActivityAndClearStack
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import app.grand.tafwak.presentation.home.HomeActivity
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LogInViewModel @Inject constructor(
  private val logInUseCase: LogInUseCase,
  val userLocalUseCase: UserLocalUseCase
) : BaseViewModel() {

  var request = LogInRequest()
  private val _logInResponse =
    MutableStateFlow<Resource<UserResponse>>(Resource.Default)
  val logInResponse = _logInResponse

  fun forgetPassword(v: View){
    v.findFragment<LogInFragment>().navigateSafe(LogInFragmentDirections.actionOpenForgotPasswordFragment())
  }

  fun login(v: View){
    if (request.phone.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_phone));
      return
    }
    if (request.phone.trim().length != 11) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_phone));
      return
    }
    if (request.password.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_password));
      return
    }
    logInUseCase.invoke(request).onEach {
      _logInResponse.value = it
    }.launchIn(viewModelScope)
  }

  fun register(v: View){
    v.findFragment<LogInFragment>().navigateSafe(LogInFragmentDirections.actionOpenSignUpFragment())
  }

  fun isCompleteProfile(): Boolean{
    val user = userLocalUseCase.invoke()
    return user.completeProfile == 1
  }
}
package app.grand.tafwak.presentation.auth.forgot_password

import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.core.extenstions.showError
import app.grand.tafwak.domain.auth.entity.model.ForgetPasswordResponse
import app.grand.tafwak.domain.auth.entity.request.ForgetPasswordRequest
import app.grand.tafwak.domain.auth.use_case.ForgetPasswordUseCase
import app.grand.tafwak.presentation.base.utils.Constants
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ForgetPasswordViewModel @Inject constructor(
  private val forgetPasswordUseCase: ForgetPasswordUseCase,
  savedStateHandle: SavedStateHandle
) :
  BaseViewModel() {
  var request = ForgetPasswordRequest()
  private val _forgetPasswordResponse =
    MutableStateFlow<Resource<ForgetPasswordResponse>>(Resource.Default)
  val forgetPasswordResponse = _forgetPasswordResponse

  //  init {
//    savedStateHandle.get<String>("email")?.let { email ->
//      request.phone = email
//    }
//
//  }
  fun forgetPassword(v: View) {
    request.type = Constants.FORGET

    if (request.phone.trim().isEmpty()) {
      v.context.showError(v.context.getString(R.string.please_enter_your_phone));
      return
    }
    if (request.phone.trim().length != 11) {
      v.context.showError(v.context.getString(R.string.please_enter_valid_phone));
      return
    }

    forgetPasswordUseCase(request)
      .onEach { result ->
        _forgetPasswordResponse.value = result
      }
      .launchIn(viewModelScope)
  }
}
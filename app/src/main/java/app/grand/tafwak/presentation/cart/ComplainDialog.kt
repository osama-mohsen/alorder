package app.grand.tafwak.presentation.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.extensions.backToPreviousScreen
import app.grand.tafwak.presentation.base.extensions.hideKeyboard
import app.grand.tafwak.presentation.base.extensions.showMessage
import app.grand.tafwak.presentation.cart.viewModel.ComplainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ReportComplainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.concurrent.schedule

@AndroidEntryPoint
class ComplainDialog : BottomSheetDialogFragment() {
  val reportArgs: ComplainDialogArgs by navArgs()
  private val viewModel: ComplainViewModel by viewModels()
  lateinit var binding: ReportComplainBinding
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    binding = DataBindingUtil.inflate(inflater, R.layout.report_complain, container, false)
    binding.viewModel = viewModel
    viewModel.request.order_id = reportArgs.orderId

    setupObserver()
    return binding.root
  }

  fun setupObserver() {
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        when (it) {
          Resource.Loading -> {
            hideKeyboard()
            viewModel.progress.set(true)
          }
          is Resource.Success -> {
            viewModel.progress.set(false)
            showMessage(it.value.message)
            backToPreviousScreen()
          }
          is Resource.Failure -> {
            viewModel.progress.set(false)
          }
        }
      }
    }

  }

  override fun onDestroy() {
    super.onDestroy()
  }

  override fun getTheme(): Int {
    return R.style.CustomBottomSheetDialogTheme;
  }
}
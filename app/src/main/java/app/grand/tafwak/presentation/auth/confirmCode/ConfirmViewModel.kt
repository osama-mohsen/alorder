package app.grand.tafwak.presentation.auth.confirmCode

import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.domain.auth.entity.model.UserResponse
import app.grand.tafwak.domain.auth.entity.request.ForgetPasswordRequest
import app.grand.tafwak.domain.auth.entity.request.VerifyAccountRequest
import app.grand.tafwak.domain.auth.use_case.ForgetPasswordUseCase
import app.grand.tafwak.domain.auth.use_case.VerifyAccountUseCase
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.Resource
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.base.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ConfirmViewModel @Inject constructor(
  private val verifyAccountUseCase: VerifyAccountUseCase,
  private val forgetPasswordUseCase: ForgetPasswordUseCase,
  savedStateHandle: SavedStateHandle
) :
  BaseViewModel() {
  val request = VerifyAccountRequest()
  private val _verifyResponse =
    MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val verifyResponse = _verifyResponse
  private val _forgetResponse = MutableStateFlow<Resource<BaseResponse>>(Resource.Default)
  val forgetResponse = _forgetResponse

  var timerText = "60:00"
  var resend = false
  lateinit var countDownTimer: CountDownTimer
  private val TAG = "ConfirmViewModel"
  private fun startTimer() {
    Log.d(TAG, "startTimer: ")
    countDownTimer = object : CountDownTimer(60000, 1000) {
      override fun onTick(millisUntilFinished: Long) {
        timerText = when {
          (millisUntilFinished / 1000) < 10 -> "0" + (millisUntilFinished / 1000)
          else -> (millisUntilFinished / 1000)
        }.toString().plus(" : 00")
        notifyChange()
        Log.d(TAG, "onTick: $timerText")
      }

      override fun onFinish() {
        resend = true
        Log.d(TAG, "onFinish: resend")
        notifyChange()
      }
    }.start()
  }


  init {
    savedStateHandle.get<String>("phone")?.let { email ->
      request.phone = email
    }
    savedStateHandle.get<String>("type")?.let { type ->
      request.type = type
    }
    savedStateHandle.get<Int>("user_id")?.let { userId ->
      request.user_id = userId
    }
    startTimer()
  }

  fun resend(v: View) {
    Log.d(TAG, "resend: ")
    resend = false
//    resendCode()
    countDownTimer.start()
    notifyChange()

//    val fragment = v.findFragment<LoginFragment>()
//    v.context.sendFirebaseSMS(fragment.requireActivity(),v,request.country_code+request.phone) { verificationId ->
//     request.verificationId = verificationId
//    }
  }


  fun verifyAccount(v: View) {
    verifyAccountUseCase(request)
      .onEach { result ->
        _verifyResponse.value = result
      }
      .launchIn(viewModelScope)
  }
}
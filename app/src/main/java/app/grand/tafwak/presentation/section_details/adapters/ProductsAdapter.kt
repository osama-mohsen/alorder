package app.grand.tafwak.presentation.section_details.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.data.product.local.ProductLocalRepository
import app.grand.tafwak.domain.home.models.Product
import com.structure.base_mvvm.R
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import app.grand.tafwak.presentation.home.viewModels.ItemProductViewModel
import com.structure.base_mvvm.databinding.ItemProductBinding
import javax.inject.Inject

class ProductsAdapter @Inject constructor(val productLocalRepository: ProductLocalRepository) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {
  var showAddButton: Boolean = true
  var clickEvent: SingleLiveEvent<Product> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
      return oldItem == newItem
    }
  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemProductViewModel(data,productLocalRepository,showAddButton)
//    holder.itemLayoutBinding.btnAdd.setOnClickListener {
//      holder.itemLayoutBinding.btnAdd.visibility = View.GONE
//      clickEvent.value = data
//      holder.itemLayoutBinding.llCounter.visibility = View.VISIBLE
//    }
    holder.setViewModel(itemViewModel)

  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

  fun insertData(insertList: List<Product>) {
    val array = ArrayList<Product>(insertList)
    array.addAll(insertList)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(null)
    differ.submitList(array)
//    notifyDataSetChanged()
    notifyItemRangeInserted(1,insertList.size)

  }

  private  val TAG = "ProductsAdapter"
  fun insertNewData(insertList: List<Product>) {
//    Log.d(TAG, "newData: ${insertList.size}")
//    Log.d(TAG, "insertNewData: ${differ.currentList.size}")
    val size = differ.currentList.size
    val list = ArrayList<Product>(differ.currentList)
    Log.d(TAG, "insertNewData: ${list.size}")
    val array = ArrayList<Product>(insertList)
    Log.d(TAG, "insertNewDataN: ${array.size}")
    list.addAll(array)
//    differ.submitList(null)
    differ.submitList(list)

    Log.d(TAG, "insertNewDataAddAll: ${array.size}")
//    Log.d(TAG, "insertNewData: ${differ.currentList.size}")
    notifyItemRangeInserted(size+1,list.size)
//    differ.submitList(null);
//    differ.submitList(array)
//    Log.d(TAG, "insertNewDataBefore: ${insertList.size}")
//    Log.d(TAG, "insertNewData: ${differ.currentList.size}")
//    notifyItemRangeInserted(0,insertList.size)
//    notifyDataSetChanged()
  }

  fun resetButtonAdd(productId: Int) {
    differ.currentList.forEachIndexed{ index , element ->
      if(element.id == productId){

      }
    }
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemProductBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemProductViewModel) {
      itemLayoutBinding.itemViewModels = itemViewModel
    }
  }

}
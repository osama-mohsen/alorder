package app.grand.tafwak.presentation.my_orders.viewModels

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.ObservableInt
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import app.grand.tafwak.domain.home.models.Category
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.order.Order
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.cart.adapters.OrderCartBodyAdapter
import app.grand.tafwak.presentation.my_orders.MyOrdersFragment
import app.grand.tafwak.presentation.my_orders.MyOrdersFragmentDirections
import com.structure.base_mvvm.BR

class ItemOrderViewModel constructor(val order: Order) : BaseViewModel() {

  fun submit(v:View){
    v.findNavController().navigate(MyOrdersFragmentDirections.actionMyOrdersFragmentToMyOrderDetailsFragment(order))
  }

  fun sendComplain(v: View){
    v.findNavController().navigate(MyOrdersFragmentDirections.actionMyOrdersFragmentToComplainDialog(order.id))
  }
}
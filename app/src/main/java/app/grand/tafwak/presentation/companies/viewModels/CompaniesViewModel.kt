package app.grand.tafwak.presentation.companies.viewModels

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import app.grand.tafwak.domain.home.models.Companies
import app.grand.tafwak.domain.home.models.CompaniesResponse
import app.grand.tafwak.domain.home.models.HomeResponse
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import app.grand.tafwak.domain.utils.Resource
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.companies.adapter.CompanyAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class CompaniesViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase
) : BaseViewModel() {


  fun setData(value: CompaniesResponse) {
    adapter.differ.submitList(value.companies)
    notifyPropertyChanged(BR.adapter)
  }

  var search = ""
  private val _response =
    MutableStateFlow<Resource<CompaniesResponse>>(Resource.Default)
  val response = _response

  init {
    callService()
  }

  fun callService(){
    homeUseCase.companies(search)
      .onEach { result ->
        _response.value = result
      }
      .launchIn(viewModelScope)
  }

  @Bindable
  val adapter: CompanyAdapter = CompanyAdapter()



}
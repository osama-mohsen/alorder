package app.grand.tafwak.presentation.search.viewModels

import androidx.databinding.Bindable
import app.grand.tafwak.domain.home.models.Product
import app.grand.tafwak.domain.home.use_case.HomeUseCase
import com.structure.base_mvvm.BR
import app.grand.tafwak.presentation.base.BaseViewModel
import app.grand.tafwak.presentation.search.adapter.SearchAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
  private val homeUseCase: HomeUseCase
) : BaseViewModel() {

  @Bindable
  val adapter: SearchAdapter = SearchAdapter()

  init {
    setupProduct()
  }

  fun setupProduct() {
    val list = ArrayList<Product>()
    adapter.differ.submitList(list)
//    notifyPropertyChanged(BR.CategoriesAdapter)
  }

}
package app.grand.tafwak.core.di

import androidx.room.Database
import androidx.room.RoomDatabase
import app.grand.tafwak.data.product.local.ProductDao
import app.grand.tafwak.domain.home.models.Product

@Database(
  entities = [Product::class],
  version = 7
)
abstract class AppDatabase : RoomDatabase() {
  abstract val getProduct: ProductDao
}
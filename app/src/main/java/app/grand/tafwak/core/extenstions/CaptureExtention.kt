package app.grand.tafwak.core.extenstions

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.fragment.findNavController
import com.structure.base_mvvm.R
import java.util.*

fun Fragment.pickImage(view: View, result: (Uri) -> Unit) {
  when {
    requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
      && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
      && requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
      pickImageViaChooser(view, result)
    }
    requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
      && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
      pickImage(false,result)
    }
    requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
      pickImage(true,result)
    }
    else -> {
      registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
      ) { permissions ->
        when {
          permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
            && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
            && permissions[Manifest.permission.CAMERA] == true -> {
            pickImageViaChooser(view, result)
          }
          permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
            && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
            pickImage(false,result)
          }
          permissions[Manifest.permission.CAMERA] == true -> {
            pickImage(true,result)
          }
          else -> {
            view.context.showInfo(getString(R.string.you_didn_t_accept_permission))
          }
        }
      }
    }
  }
}


private fun Fragment.pickImageViaChooser(view: View, result: (Uri) -> Unit) {
  val camera = view.context.getString(R.string.camera)
  val gallery = view.context.getString(R.string.gallery)

  view.showPopup(listOf(camera, gallery)) {
    pickImage(it.title?.toString() == camera, result)
  }
}


private fun Fragment.pickImage(fromCamera: Boolean, result: (Uri) -> Unit) {
  if (fromCamera) {
    registerForActivityResult(
      ActivityResultContracts.StartActivityForResult()
    ) {
      if (it.resultCode == Activity.RESULT_OK) {
        val uri = it.data?.data ?: return@registerForActivityResult
        result(uri)
      }
    }.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
  } else {
    registerForActivityResult(
      ActivityResultContracts.StartActivityForResult()
    ) {
      if (it.resultCode == Activity.RESULT_OK) {
        val uri = it.data?.data ?: return@registerForActivityResult
        result(uri)
      }
    }.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
  }
}
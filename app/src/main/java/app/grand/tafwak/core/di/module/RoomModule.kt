package app.grand.tafwak.core.di.module

import android.content.Context
import androidx.room.Room
import app.grand.tafwak.core.di.AppDatabase
import app.grand.tafwak.data.product.local.ProductLocalDataSource
import com.structure.base_mvvm.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

//  val MIGRATION_1_2: Migration = object : Migration(1, 2) {
//    override fun migrate(database: SupportSQLiteDatabase) {
//      database.execSQL(
//        "ALTER TABLE users "
//          + "ADD COLUMN address TEXT"
//      )
//    }
//  }
  @Provides
  @Singleton
  fun provideMyDB(@ApplicationContext context: Context): AppDatabase {


    return Room.databaseBuilder(
      context,
      AppDatabase::class.java,
      BuildConfig.ROOM_DB
    )
      .fallbackToDestructiveMigration()
//      .addMigrations(MIGRATION_1_2)
      .build()
  }

  @Provides
  @Singleton
  fun provideProductRepository(db: AppDatabase): ProductLocalDataSource {
    return ProductLocalDataSource(db.getProduct)
  }

}




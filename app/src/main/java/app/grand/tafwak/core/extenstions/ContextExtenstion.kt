package app.grand.tafwak.core.extenstions

import android.content.Context
import android.content.pm.PackageManager
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import com.structure.base_mvvm.R

fun Context.dpToPx(value: Float): Float {
  return TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics
  )
}

/** - Layout inflater from `receiver`, by using [LayoutInflater.from] */
val Context.layoutInflater: LayoutInflater
  get() = LayoutInflater.from(this)

/**
 * - Inflates a layout from [layoutRes] isa.
 *
 * @param parent provide [ViewGroup.LayoutParams] to the returned root view, default is `null`
 * @param attachToRoot if true then the returned view will be attached to [parent] if not `null`,
 * default is false isa.
 *
 * @return rootView in the provided [layoutRes] isa.
 */
@JvmOverloads
fun Context.inflateLayout(
  @LayoutRes layoutRes: Int,
  parent: ViewGroup? = null,
  attachToRoot: Boolean = false
): View {
  return layoutInflater.inflate(layoutRes, parent, attachToRoot)
}


fun View.showPopup(list: List<String>, context: Context = this.context, listener: (MenuItem) -> Unit) {
  val popupMenu = PopupMenu(context, this)

  popupMenu.inflate(R.menu.menu_empty)
  for (item in list) {
    popupMenu.menu.add(item)
  }

  popupMenu.setOnMenuItemClickListener {
    listener(it)

    false
  }

  popupMenu.show()
}


fun Context.checkSelfPermissionGranted(permission: String): Boolean {
  return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}
